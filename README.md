# Valmir Core (Laravel4 Package)

Core provider é um pacote de soluções prontas, voltas para criação de APIs



## Quick start

**PS:** 

### Required setup

Em seu `composer.json`  adicionar dentro da key `require` o seguinte valor:

    "valmir/core": "dev-master"

Ainda se preferir recomendamos algumas packagen para auxiliar no desenvolvimento. para utilizar inclua no seu requere-dev:
```json
"require-dev": {
      "phpunit/phpunit": "3.7.*",
      "way/generators": "~2.0",
      "fzaninotto/Faker": "1.3.*",
      "barryvdh/laravel-ide-helper": "1.11.*@dev",
      "itsgoingd/clockwork": "~1.3",
      "mockery/mockery" : "0.9.*@dev"
}
```    

Execute comando Composer update 

    $ composer update

Em seu `config/app.php` dentro do array `$providers`  adicione `'Valmir\Core\CoreServiceProvider'`, EX:

```php
'providers' => array(

    'Illuminate\Foundation\Providers\ArtisanServiceProvider',
    'Illuminate\Auth\AuthServiceProvider',
    ...
    'Valmir\Core\CoreServiceProvider',//Meu Modulo Core

),
```

Em seu `config/app.php` dentro do array `$aliases` adicione `'Entrust'    => 'Zizaco\Entrust\EntrustFacade'`, EX:

```php
'aliases' => array(

    'App'        => 'Illuminate\Support\Facades\App',
    'Artisan'    => 'Illuminate\Support\Facades\Artisan',
    ...
    'ResponseCors' => 'Valmir\Core\Facades\ResponseCors',

),
```

### Configuration

Execute comando  

    $ php artisan config:publish valmir/core

Edite o arquivo `config/app/packages/valmir/core`    

### Migrates

Para rodas as migration

    $ php artisan migrate --package=valmir/core

Esse comando ira criar as tabelas:
- Users
- password_reminders
- user_logs
- Entrust, sistema de nivel de acesso, [Veja a documentação](https://github.com/Zizaco/entrust) para ter mais informações.

### Controllers API

Criando um axemplo de controller

```php
<?php

use \Valmir\Core\BaseApiController;

class PessoaServiceController extends BaseApiController
{

}
```

O `BaseApiController` permite utilizar varios metodos de retornos como `respondError` ...

## Usage

### Concepts


```php
public function show($id){
    
    $result = $this->repository->find($id);
    if( is_null($result) ){
        return $this->errorNotFound('Not FOUD');
    }

    return $this->respondWithItem( $result, new PessoaTransformer() );
}

```


## Features

**Current:**
- list

**Planned:**
- Config core.enable_cors = false
- Criar uma Trait com um monte de testes padrÃ£o: registra, update, excluir, teste unique, busca ...
- separa  em modulos menores com load PSR-0 ex: cidades, themes, entrust, pagamentos, envoices (componentizar geral) salvar tudo dentro do bitbuket  registrar os `issues` la dentro


Acesse [checklist completo](https://bitbucket.org/valmirw3/lara-core/src/master/docs/checklist.md)


## Release Notes

### Version 1.1
* Init documentação
* Remove AuthRepository e create AuthLogin



## License

Private, somentes amigos com acesso ao [repositório](https://bitbucket.org/valmirw3/lara-core/)

## Additional information

Acesse a [documentação](https://bitbucket.org/valmirw3/lara-core/src/master/docs/)
