<?php


use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;


//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

//Faker\Factory::create();

/**
 * Features context.
 */
class BaseFeatureContext extends BehatContext
{
    /**
     * The Guzzle HTTP Client.
     */
    protected $client;

    /**
     * The current resource
     */
    protected $resource;

    /**
     * The request payload
     */
    protected $requestPayload;

    /**
     * The Guzzle HTTP Response.
     */
    protected $response;

    /**
     * The decoded response object.
     */
    protected $responsePayload;

    /**
     * The current scope within the response payload
     * which conditions are asserted against.
     */
    protected $scope;
    
    
    /**
     * Usado para passar parametros nos headers
     * @var String 
     */
    protected $header_token;


    /**
     * Dados de acesso para o login
     * @var array
     */
    protected $user_auth = array(
        'email'    => 'valmir.php@gmail.com',
        'password' => 'webmaster'
    );

    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $config = isset($parameters['guzzle']) && is_array($parameters['guzzle']) ? $parameters['guzzle'] : [];

        $config['base_url'] = $parameters['base_url'];
        $config['cookies'] = true;
        
        
        date_default_timezone_set('America/Sao_Paulo');
        $this->client = new Client($config);    
        
        $this->header_token = ConfigBehat::get('header_token');
        
        if( !empty($this->header_token) ){
            $this->client->setDefaultOption('headers', array('ng-csrf-token' => $this->header_token));
        }
        
    }

    /**
     * @Given /^I have the payload:$/
     */
    public function iHaveThePayload(PyStringNode $requestPayload)
    {
        $this->requestPayload = $requestPayload;
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE) ([^"]*)"$/
     */
    public function iRequest($httpMethod, $resource)
    {
        $this->resource = $this->scopeReplaceURL( $resource );

        $method = strtolower($httpMethod);
        
        try {
            switch ($httpMethod) {
                case 'PUT':
                case 'POST':
                    $this->response = $this
                        ->client
                        ->$method($this->resource, null, $this->requestPayload);
                    break;

                default:
                    $this->response = $this
                        ->client
                        ->$method($this->resource, [ 'cookies' => true]);
            }
        } catch (BadResponseException $e) {

            $response = $e->getResponse();
            echo "\n URL ERROR: {$this->resource} \n";
            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
    }
    

     /**
     * @When /^I request with data "(PUT|POST|DELETE) ([^"\{]*) ([^"]*)"$/
     */
    public function iRequestWithData($httpMethod, $resource, $data)
    {
        $data = $this->trataScope($data);
        $this->resource = $this->scopeReplaceURL( $resource );
        $method = strtolower($httpMethod);
        
        if( json_last_error() ){
            throw new Exception('Invalid JSON: ' . json_last_error_msg() );
        }
        
        try {
            
            $this->response = $this->client
                    ->$method($this->resource, ['body' => $data, 'cookies' => true ]);
              
        } catch (BadResponseException $e) {

            $response = $e->getResponse();
            echo "\n URL ERROR: {$this->resource} \n" . json_encode($data) . "\n";
            
            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
    }

    
    /**
     * @Given /^the "([^"]*)" property set in ConfigBehat "([^"]*)"$/
     */
    public function thePropertySetConfig($property, $key)
    {
        
        $payload = $this->getScopePayload();
        $value = $this->arrayGet($payload, $property);

        assertNotNull( 
                $value, 
                'valor não encontrado para salvar em ConfigBehat' 
        );
        
        ConfigBehat::set($key, $value);                
    }
    
    
    /**
     * @Then /^I get a "([^"]*)" response$/
     */
    public function iGetAResponse($statusCode)
    {
        $response = $this->getResponse();
        $contentType = $response->getHeader('Content-Type');

        if ($contentType === 'application/json') {
            $bodyOutput = $response->getBody();
        } else {
            $bodyOutput = 'Output is '.$contentType.', which is not JSON and is therefore scary. Run the request manually.';
        }
        assertSame((int) $statusCode, (int) $this->getResponse()->getStatusCode(), $bodyOutput);
    }

    /**
     * @Given /^the "([^"]*)" property equals "([^"]*)"$/
     */
    public function thePropertyEquals($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        assertEquals(
            $actualValue,
            $expectedValue,
            "Asserting the [$property] property in current scope equals [$expectedValue]: ".json_encode($payload)
        );


    }

    /**
     * @Given /^the "([^"]*)" property exists$/
     */
    public function thePropertyExists($property)
    {
        $payload = $this->getScopePayload();

        $message = sprintf(
            'Asserting the [%s] property exists in the scope [%s]: %s',
            $property,
            $this->scope,
            json_encode($payload)
        );

        if (is_object($payload)) {
            assertTrue(array_key_exists($property, get_object_vars($payload)), $message);

        } else {
            assertTrue(array_key_exists($property, $payload), $message);
        }
    }

    /**
     * @Given /^the "([^"]*)" property is an array$/
     */
    public function thePropertyIsAnArray($property)
    {
        $payload = $this->getScopePayload();

        $actualValue = $this->arrayGet($payload, $property);

        assertTrue(
            is_array($actualValue),
            "Asserting the [$property] property in current scope [{$this->scope}] is an array: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is an object$/
     */
    public function thePropertyIsAnObject($property)
    {
        $payload = $this->getScopePayload();

        $actualValue = $this->arrayGet($payload, $property);

        assertTrue(
            is_object($actualValue),
            "Asserting the [$property] property in current scope [{$this->scope}] is an object: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is an empty array$/
     */
    public function thePropertyIsAnEmptyArray($property)
    {
        $payload = $this->getScopePayload();
        $scopePayload = $this->arrayGet($payload, $property);

        assertTrue(
            is_array($scopePayload) and $scopePayload === [],
            "Asserting the [$property] property in current scope [{$this->scope}] is an empty array: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is not empty/
     */
    public function thePropertyIsNotEmpty($property)
    {
        $payload = $this->getScopePayload();
        $scopePayload = $this->arrayGet($payload, $property);

        assertTrue(
            empty($scopePayload) === false,
            "Asserting the [$property] property in current scope [{$this->scope}] is empty ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property contains (\d+) items$/
     */
    public function thePropertyContainsItems($property, $count)
    {
        $payload = $this->getScopePayload();

        assertCount(
            $count,
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property contains [$count] items: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is an integer$/
     */
    public function thePropertyIsAnInteger($property)
    {
        $payload = $this->getScopePayload();

        isType(
            'int',
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property in current scope [{$this->scope}] is an integer: ".json_encode($payload)
        );
    }
    
    

    /**
     * @Given /^the "([^"]*)" property is a string$/
     */
    public function thePropertyIsAString($property)
    {
        $payload = $this->getScopePayload();

        isType(
            'string',
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property in current scope [{$this->scope}] is a string: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a string equalling "([^"]*)"$/
     */
    public function thePropertyIsAStringEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();

        $this->thePropertyIsAString($property);

        $actualValue = $this->arrayGet($payload, $property);

        assertSame(
            $actualValue,
            $expectedValue,
            "Asserting the [$property] property in current scope [{$this->scope}] is a string equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a boolean$/
     */
    public function thePropertyIsABoolean($property)
    {
        $payload = $this->getScopePayload();

        assertTrue(
            gettype($this->arrayGet($payload, $property)) == 'boolean',
            "Asserting the [$property] property in current scope [{$this->scope}] is a boolean."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a boolean equalling "([^"]*)"$/
     */
    public function thePropertyIsABooleanEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        if (! in_array($expectedValue, ['true', 'false'])) {
            throw new \InvalidArgumentException("Testing for booleans must be represented by [true] or [false].");
        }

        $this->thePropertyIsABoolean($property);

        assertSame(
            $actualValue,
            $expectedValue == 'true',
            "Asserting the [$property] property in current scope [{$this->scope}] is a boolean equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a integer equalling "([^"]*)"$/
     */
    public function thePropertyIsAIntegerEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        $this->thePropertyIsAnInteger($property);

        assertSame(
            $actualValue,
            (int) $expectedValue,
            "Asserting the [$property] property in current scope [{$this->scope}] is an integer equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is either:$/
     */
    public function thePropertyIsEither($property, PyStringNode $options)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        $valid = explode("\n", (string) $options);

        assertTrue(
            in_array($actualValue, $valid),
            sprintf(
                "Asserting the [%s] property in current scope [{$this->scope}] is in array of valid options [%s].",
                $property,
                implode(', ', $valid)
            )
        );
    }

    /**
     * @Given /^scope into the first "([^"]*)" property$/
     */
    public function scopeIntoTheFirstProperty($scope)
    {
        $this->scope = "{$scope}.0";
    }

    /**
     * @Given /^scope into the "([^"]*)" property$/
     */
    public function scopeIntoTheProperty($scope)
    {        
        $this->scope = $scope;
    }

    /**
     * @Given /^the properties exist:$/
     */
    public function thePropertiesExist(PyStringNode $propertiesString)
    {
        foreach (explode("\n", (string) $propertiesString) as $property) {
            $this->thePropertyExists($property);
        }
    }

    /**
     * @Given /^reset scope$/
     */
    public function resetScope()
    {
        $this->scope = null;
    }

    /**
     * @Transform /^(\d+)$/
     */
    public function castStringToNumber($string)
    {
        return intval($string);
    }

    /**
     * @Given /^ConnectAuth$/
     */
    public function connectAuth(){

        $data = json_encode($this->user_auth);
        $this->iRequestWithData('POST', '/service/v1/authenticate/login', "$data" );
        $this->iGetAResponse(200);
        $this->scopeIntoTheProperty('data.user');
        $this->thePropertySetConfig('token', 'header_token');
    }

    /**
     * Busca um ID e salva no scope
     * @Given /^request scope "([^"]*)" in "(GET|PUT|POST|DELETE) ([^"\{]*)" set "([^"]*)"$/
     */
    public function buscaIdResource($name_colun, $httpMethod, $resource, $name_scope){

        $this->iRequest($httpMethod, $resource );
        $this->iGetAResponse(200);
        $this->scopeIntoTheProperty($name_colun);
        $value = $this->getScopePayload();

        assertNotNull(
            $value,
            'valor não encontrado para salvar em ConfigBehat o REQUEST SCOPE'
        );

        ConfigBehat::set($name_scope, $value);
    }



    protected function trataScope($strJson){
        $strJson = str_replace("\'", '"', $strJson);
        $data = json_decode(trim($strJson), true);

        if( strpos($strJson, '$')  === false ) {
            return $data;
        }

        $data = $this->scopeToJson($data);
        $data = $this->scopeToFaker($data);
        $data = $this->scopeToFile($data);

        return $data;
    }

    /**
     *  Subistitui $scope por a variavel armazenada em ConfigBehat, EX:
     *  When I request with data "POST /estado {'estado': 'Paraná', 'uf': 'PR', 'pais_id' : '$scope(pais_id)'}"
     */
    protected function scopeToJson($data){

        foreach( $data as $key=>$d ){

            if( is_string($d) ) {
                preg_match('/\$scope\((.*)\)/', $d, $math);

                if (count($math) == 2) {
                    $key_property = $math[1];
                    $data[$key] = ConfigBehat::get($key_property);
                }

            }else if(is_array($d)){
                $data[$key] = $this->scopeToJson($d);
            }

        }
        
        return $data;
    }

    /**
     *  Subistitui $faker por a variavel gerada pelo Faker\Factory, EX:
     *  When I request with data "POST /aluno {'nome': '$faker->name'}"
     */
    protected function scopeToFaker($data){
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create();

        foreach( $data as $key=>$var ){

            if(is_string($var)) {
                if (strpos($var, '$faker->') === 0) {

                    $r = false;
                    eval('$r = ' . $var . '; ');
                    $data[$key] = $r;

                    if ($data[$key] instanceof DateTime) {
                        $data[$key] = $data[$key]->format('Y-m-d H:i:s');
                    }

                }
            }else if(is_array($var)){
                $data[$key] = $this->scopeToFaker($var);
            }
        }

        return $data;
    }

    /**
     *  Carraga um arquivo para upload no lugar do file
     *  When I request with data "POST /aluno {'nome': '$file(path)'}"
     */
    protected function scopeToFile($data){
        foreach( $data as $key=>$d ){

            if( is_string($d) ) {
                preg_match('/\$file\((.*)\)/', $d, $math);

                if (count($math) == 2) {
                    $key_property = $math[1];

                    if(file_exists($key_property)){
                        $data[$key] = fopen($key_property, 'r');
                    }

                }

            }else if(is_array($d)){
                $data[$key] = $this->scopeToJson($d);
            }

        }

        return $data;
    }
    
    /**
     *  Subistitui $scope por a variavel armazenada em ConfigBehat, EX:
     *   When I request with data "PUT /service/v1/pais/$scope(pais_id) {'pais' : 'pais update'}"
     */
    protected function scopeReplaceURL($url){

            preg_match_all('/\$scope\(([^\)]*)\)/', $url, $math);

            if(count($math) == 2){

                foreach( $math[1] as $key_property ) {
                    $replacement = ConfigBehat::get($key_property);
                    $url = str_replace("\$scope({$key_property})", $replacement, $url);
                }

            }
            
        return $url;
    }

        /**
     * Checks the response exists and returns it.
     *
     * @return  Guzzle\Http\Message\Response
     */
    protected function getResponse()
    {
        if (! $this->response) {
            throw new Exception("You must first make a request to check a response.");
        }

        return $this->response;
    }

    /**
     * Return the response payload from the current response.
     *
     * @return  mixed
     */
    protected function getResponsePayload()
    {
        if (! $this->responsePayload) {
            $json = json_decode($this->getResponse()->getBody(true));

            if (json_last_error() !== JSON_ERROR_NONE) {
                $message = 'Failed to decode JSON body ';

                switch (json_last_error()) {
                    case JSON_ERROR_DEPTH:
                        $message .= '(Maximum stack depth exceeded).';
                        break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $message .= '(Underflow or the modes mismatch).';
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        $message .= '(Unexpected control character found).';
                        break;
                    case JSON_ERROR_SYNTAX:
                        $message .= '(Syntax error, malformed JSON).';
                        break;
                    case JSON_ERROR_UTF8:
                        $message .= '(Malformed UTF-8 characters, possibly incorrectly encoded).';
                        break;
                    default:
                        $message .= '(Unknown error).';
                        break;
                }

                throw new Exception($message);
            }

            $this->responsePayload = $json;
        }

        return $this->responsePayload;
    }

    /**
     * Returns the payload from the current scope within
     * the response.
     *
     * @return mixed
     */
    protected function getScopePayload()
    {
        $payload = $this->getResponsePayload();

        if (! $this->scope) {
            return $payload;
        }

        return $this->arrayGet($payload, $this->scope);
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @copyright   Taylor Otwell
     * @link        http://laravel.com/docs/helpers
     * @param       array   $array
     * @param       string  $key
     * @param       mixed   $default
     * @return      mixed
     */
    protected function arrayGet($array, $key)
    {
        if (is_null($key)) {
            return $array;
        }

        // if (isset($array[$key])) {
        //     return $array[$key];
        // }

        foreach (explode('.', $key) as $segment) {

            if (is_object($array)) {
                if (! isset($array->{$segment})) {
                    return;
                }
                $array = $array->{$segment};

            } elseif (is_array($array)) {
                if (! array_key_exists($segment, $array)) {
                    return;
                }
                $array = $array[$segment];
            }
        }

        return $array;
    }

    protected function debugException(){
        print_r( $this->getResponsePayload() );
        //echo "\n URL ERROR: {$this->resource} \n $data \n";
    }
}


class ConfigBehat{
    protected static $var_confi;
    
    public static function get($key, $defaul = null){
        return  isset(self::$var_confi[$key]) ? self::$var_confi[$key] : $defaul;
    }
    
    public static function set($key, $value){
        self::$var_confi[$key] = $value;
    }
}

/**
 * @param string $func - method name
 * @param object $obj - object to call method on
 * @param boolean|array $params - array of parameters
 */
function call_object_method_array($func, $obj, $params=false){
    if (!method_exists($obj,$func)){
        // object doesn't have function, return null
        return (null);
    }
    // no params so just return function
    if (!$params){
        return ($obj->$func());
    }
    // build eval string to execute function with parameters
    $pstr='';
    $p=0;
    foreach ($params as $param){
        $pstr.=$p>0 ? ', ' : '';
        $pstr.='$params['.$p.']';
        $p++;
    }
    $evalstr='$retval=$obj->'.$func.'('.$pstr.');';
    $evalok=eval($evalstr);
    // if eval worked ok, return value returned by function
    if ($evalok){
        return ($retval);
    } else {
        return (null);
    }
    return (null);
}