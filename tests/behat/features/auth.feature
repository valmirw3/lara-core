Feature: Auth

Scenario: Registrar conta - dados invalido
    When I request with data "POST /service/v1/authenticate/registrar {'email': 'tddail.com', 'password':'1234'}"
    Then I get a "400" response
    And scope into the "error.validation" property
    And the properties exist:
            """
            nome
            email
            password_confirmation
            """


Scenario: Registrar conta - SUCCESS
    When I request with data "POST /service/v1/authenticate/registrar {'nick' : 'TDD1', 'codigo' : 'WWWW', 'nome' : 'TDD', 'email': 'tdd@gmail.com', 'password':'1q2w3e4r', 'password_confirmation' : '1q2w3e4r'}"
    Then I get a "200" response
    And scope into the "data" property
    And the "email" property equals "tdd@gmail.com"


Scenario: Registrar conta - tentar usar um email ou nick são unico                   
    When I request with data "POST /service/v1/authenticate/registrar {'nick' : 'TDD1', 'codigo' : 'WWWW', 'nome' : 'TDD', 'email': 'tdd@gmail.com', 'password':'1q2w3e4r', 'password_confirmation' : '1q2w3e4r'}"
    Then I get a "400" response
    And scope into the "error.validation.email" property
    And scope into the "error.validation.nick" property



Scenario: Efetuando Login 
    When I request with data "POST /service/v1/authenticate/login {'email': 'tdd@gmail.com', 'password':'1q2w3e4r'}"
    Then I get a "200" response
    And scope into the "data.user" property        
        And the properties exist:
            """
            id
            """
        And the "id" property is an integer


Scenario: Verifica se o user esta logado e retorna os dados do user
    When I request "GET /service/v1/authenticate/user"
    Then I get a "200" response
    And scope into the "data" property
        And the properties exist:
            """
            nome
            email
            """
        And the "email" property equals "tdd@gmail.com"
        

Scenario: Efetuando Logout e tenta acessar novamente
    When I request "GET /service/v1/authenticate/logout"
    Then I get a "200" response
        And the properties exist:
            """
            status
            message
            """
        And the "status" property equals "success"
        And the "message" property equals "Disconectado!"
            When I request "GET /service/v1/authenticate/user"
            Then I get a "403" response
