<?php

/**
 * Modelo inicial dos testes BEHAT
 */
require_once __DIR__.'/../../../../../vendor/valmir/core/tests/behat/features/FeatureContext.php';
require_once __DIR__.'/../../../../../vendor/phpunit/phpunit/PHPUnit/Autoload.php';
require_once __DIR__.'/../../../../../vendor/phpunit/phpunit/PHPUnit/Framework/Assert/Functions.php';


class FeatureContext extends BaseFeatureContext
{

}