Scenario: Registrar com FB
    When I request with data "POST /api/v1/auth/register?embed=pessoa.rede_fb {'nome':'Valmir barbosa', 'email': '$faker->email', 'password':'webmaster', 'rede_social': {'name_rede':'facebook', 'profile':'1234', 'email_perfil':'$faker->email', 'foto_perfil':'$faker->imageUrl(\'250\', \'250\', \'cats\')'} }"
    Then I get a "201" response
      And scope into the "data.pessoa.data.rede_fb.data" property
      And the "profile" property equals "1234"

Scenario: Registrar com upload de foto
    When I request with data "POST /api/v1/auth/register {'nome':'Valmir barbosa', 'email': '$faker->email', 'password':'1234', 'file':'$file(/home/valmir/Imagens/coisas/wall2.jpg)'}"
    Then I get a "201" response