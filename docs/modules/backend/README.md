#Backend

> *Este modulo depende do Valmir/Core*

# Composer.json
```json
"require": {
    ....
    "valmir/core" : "dev-master",
    "valmir/Backend" : "dev-master"
},
"repositories": [
    {
            "...." 
    },
    {
        "type": "git",
        "url": "git@bitbucket.org:valmirw3/lara-core-backend.git"
    }
],
```

# App/config/app.php

```php
<?php
	'providers' => array(
        'Valmir\Core\ServiceProvider',//Meu Modulo Core
  	    'Valmir\Backend\BackendServiceProvider',//Meu Modulo Back-end  	        

```

###Publish configs
```bash
php artisan config:publish valmir/backend
```

###Publish assets
```bash
php artisan asset:publish "valmir/backend"
```

### Routes ###
```php
<?php //routes.php

Route::group(array('prefix' => 'admin', 'namespace' => 'MyApp\Admin', 'before' => "BackendAuth|serviceCan:access_admin"), function() 
{

    //Enable Module AgularJS
    vBackendHelper::$ngApp = "MyApp";

    Route::get('/', 'DashController@index');//ex

});



/*******************
 * AUTH            *
 *******************/
Route::group(array('prefix' => 'auth', 'namespace' => 'Valmir\Backend'), function() {
    //// Chama os controller do Backend
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@formLogin']);
    Route::post('login','AuthController@doLogin');
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
});

```

---------------------------

## Libs Angular
- http://vitalets.github.io/angular-xeditable/#editable-row
- http://bazalt-cms.com/ng-table/example/23
- https://github.com/jirikavi/AngularJS-Toaster
- http://angular-ui.github.io/bootstrap/
- https://github.com/danialfarid/angular-file-upload
