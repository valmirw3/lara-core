<?php

class PermissionsTableSeeder extends Seeder {

    public function run() {

        $this->truncates();

        $this->seedPermissions();
        $this->seedRoles();
        $this->attachUserWebmaster();

    }

    protected function seedRoles()
    {
        //myTodo criar todas as roles defaul do sistema
        $roles = array(
            array(
                'name' => 'webmaster',
                'display_name' => 'Webmaster',
            ),
            array(
                'name' => 'administrador',
                'display_name' => 'Administrador',
            ),
            array(
                'name' => 'designer',
                'display_name' => 'Designer',
            ),
            array(
                'name' => 'financeiro',
                'display_name' => 'Financeiro',
            ),
            array(
                'name' => 'vendedor_de_franquia',
                'display_name' => 'Vendedor de Franquia',
            ),
            array(
                'name' => 'perfil_franqueado',
                'display_name' => 'Perfil Franqueado',
            ),
        );

        DB::table('roles')->insert($roles);

    }

    protected function seedPermissions()
    {
        //myTodo criar todas as permissions defaul do sistema
        $permissions = array(

            #CHAMADO
            array('name' => 'abrir_chamado', 'display_name' => 'Abrir chamado', 'group_name' => 'Chamados'),
            array('name' => 'responder_chamado', 'display_name' => 'Responder chamados', 'group_name' => 'Chamados'),


            #SISTEMA
            array('name' => 'gerenciar_permissoes', 'display_name' => 'Gerenciar Permissões', 'group_name' => 'Sistema'),

        );

        DB::table('permissions')->insert($permissions);

    }


    protected function attachUserWebmaster()
    {
        $webmaster = Role::where('name', 'webmaster')->first();
        $user = \User::where('nome', '=', 'admin')->first();

        if($user && $webmaster){

            //add all permissions a ROLE webmaster
            $permissions = Permission::all()->lists('id');
            $webmaster->perms()->sync( $permissions );

            $user->attachRole($webmaster);
        }

    }



    protected function truncates()
    {
        DB::table('permission_role')->delete();
        DB::table('assigned_roles')->delete();
        DB::table('permissions')->delete();
        DB::table('roles')->delete();
    }

}
