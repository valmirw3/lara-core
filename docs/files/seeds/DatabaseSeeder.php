<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

        if (App::environment() === 'production') {
            exit('Acabei de salvar sua vida :)');
        }

		Eloquent::unguard();

        $this->trucateTables();

        $this->call('UserTableSeeder');

	}

    /**
     * Limpa todas as tabelas definidas no array
     */
    public function trucateTables(){
        $truncate = [
            'user_logs',
            'users',
        ];

        foreach ($truncate as $table) {
            DB::table($table)->delete();
        }
    }

}
