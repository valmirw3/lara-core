<?php

use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

         User::create([
                'nome'               => 'Valmir',
                'email'              => 'valmir.php@gmail.com',
                'password'           => \Hash::make('webmaster'),
            ]);

        User::create([
                'nome'               => 'teste',
                'email'              => 'teste@gmail.com',
                'password'           => \Hash::make('teste'),
            ]);

        User::create([
            'nome'               => 'admin',
            'email'              => 'admin@admin.com',
            'password'           => \Hash::make('admin'),
        ]);
                
         //$faker = Faker::create();
//        for ($i = 0; $i < 10; $i++) {
//            User::create([
//                'nome'               => $faker->name,
//                'email'              => $faker->email,
//                'password'           => \Hash::make('webmaster'),
//            ]);
//        }
    }

}