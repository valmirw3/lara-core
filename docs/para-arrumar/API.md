#Manual API
> 
O motivo deste documento é monstrar as principais convenções e padrões usados no projeto.

#1. Semear dados
Usar o Seed Junto com o Fratctal 

#2. Requerimentos Funcionais
**EX: Locais**
- Criar
- Ler
- Atualizar
- Excluir
- Listar (lat, lon, distância ou caixa)

##2.1 Pontos de acesso
Ação | Ponto de Acesso | Rota
Criar | POST /users |  Route::post(‘users’, ‘UsersController@create’);

#3. Saida do código

```json
{
    "data": {
        "name": "Phil Sturgeon",
        "id": "511501255",
        "comments": {
            "data": [
                {
                    "id": 123423
                    "text": "MongoDB is web-scale!"
                }
            ]
        }    
    }
}
```


#4. Código do ERRO
- 200 - Genérico para tudo que está ok
- 201 - Algo foi criado
- 202 - Aceito e sendo processado assincronamente (codificação de vídeos, redimensiona-
mento de imagens e etc)
- 400 - Argumentos errados (validação ausente)
- 401 - Não autorizado (deve existir um usuário atual)
- 403 - Usuário atual não autorizado a acessar esta informação
- 404 - A URL não é uma rota válida ou o recurso solicitado não existe
- 410 - A informação foi excluída, desativada, suspensa e etc
- 405 - Método não permitido (seu framework provavelmente cuidará disso pra você)
- 500 - Algo inesperado aconteceu e é culpa da API
- 503 - API indisponível no momento, por favor tente mais tarde

#5 TESTES TDD
```bash
cd /path/to/my/app
mkdir tests && mkdir tests/behat
cd tests/behat
../../bin/behat --init
```

##5.1 Cria tabela de acesso
Ação | Ponto de Acesso | Funcionalidades
Criar | POST /users | features/users.feature
