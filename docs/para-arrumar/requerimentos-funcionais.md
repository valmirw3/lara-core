- [#Auth](#auth)  
- [#Pais](#pais)  
- [#Estado](#estado)
- [#Cidade](#cidade)





------------------------------------
<a id="auth"></a>
#1. Auth

##1.1 Requiremento
- Login
- Logout
- Recuperar senha (informa o email e recebe um token no email)
- Resetar senha (cria uma nova senha)
- Exibir dados do user logado

##1.2 Ponto de acessos
**Namespace:** Valmir\Core
**BaseUrl:** service/v1/authenticate  

 Ação | Ponto de Acesso   | Rota |   |
------|---------|--------|---
Logar | POST /login  |  AuthenticationController@login  | [X]
Logout | GET /logout  |  AuthenticationController@logout  | [X]
Registrar | POST /registrar  |  AuthenticationController@registrar  | [X]
Recuperar senha | POST /remind  |  AuthenticationController@remind  | [ ]
Recuperar senha | PUT /reset  |  AuthenticationController@reset  | [ ]
Dados user logado | GET /reset  |  AuthenticationController@getUser  | [X]

   
------------------------------------
<a id="pais"></a>
#2.Pais

##2.1 Requiremento
- Listar
- Cadastrar
- Alterar
- Excluir
- listar estados do pais

##2.2 Premissas
- Um pais deve ser unico no banco


##2.3 Ponto de acessos
**Namespace:** Corae/Admin
**BaseUrl:** service/v1/pais  

 Ação | Method | Ponto de Acesso   | Rota |   | 
------|--------|---------|--------|--------|---| 
Listar | GET| / |  PaisController@index  | [X]
Cadastrar | POST| / |  PaisController@store  | [X]
Alterar | PUT| /{id} |  PaisController@update  | [X]
Excluir | DELETE| /{id} |  PaisController@destroy  | [X]
Listar Estados | GET| /{pais_id}/?embed=estados |  PaisController@listarEstados  | [X]


------------------------------------
<a id="estado"></a>
#3.Estado

##3.1 Requiremento
- Listar
- Cadastrar
- Alterar
- Excluir
- Listar Cidades do estado


##3.2 Ponto de acessos
**Namespace:** Corae/Admin 
**BaseUrl:** service/v1/estado  

 Ação | Method| Ponto de Acesso   | Rota |   |
------|-------|-------------------|----- | ---| 
Listar | GET | / |  EstadoController@listar  | [X]
Cadastrar | POST | / |  EstadoController@store  | [X]
Alterar | PUT | /{id} |  EstadoController@update  | [X]
Excluir | DELETE | /{id} |  EstadoController@destroy  | [X]
Listar Cidades | GET| /{estado_id}/?embed=cidades |  EstadoController@listarCidades  | [X]


------------------------------------

<a id="cidade"></a>
#4.Cidade

##4.1 Requiremento
- Listar
- Cadastrar
- Alterar
- Excluir


##4.2 Ponto de acessos
**Namespace:** Corae/Admin 
**BaseUrl:** service/v1/estado  

 Ação | Method| Ponto de Acesso   | Rota |   |
------|-------|-------------------|----- | ---| 
Listar | GET | / |  CidadeController@listar  | [ ]
Cadastrar | POST | / |  CidadeController@store  | [ ]
Alterar | PUT | /{id} |  CidadeController@update  | [ ]
Excluir | DELETE | /{id} |  CidadeController@destroy  | [ ]


------------------------------------ 




