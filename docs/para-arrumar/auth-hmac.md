Auth HMAC
==========================================

Novo filtro para app
`core.auth.hmac`


Basta ativar em config/package/valmir/core/core.php

```php
array(

    'auth_hmac' => array(
        'enable_auth' => true,
        'private_key' => 'sua private key'
    )
    
```
