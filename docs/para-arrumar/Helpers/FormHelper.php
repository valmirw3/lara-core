<?php

class FormHelper {
    /*
     * Trata qual valor ira retornar como valor para input comparando os dados do banco com os do submit form
     */

    public static $clalsInput = 'form-control';
    public static $model = null;
    public static $attributes = array();
    public static $showMsg = true;

    public static function InputOld($key, $model = null) {

        if (is_null($model))
            $model = static::$model;

        $value = null;
        $old = Input::old($key);

        if (!empty($old)) {
            $value = $old;
        } else if (isset($model->{$key})) {
            $value = $model->{$key};
        }

        return $value;
    }

    public static function PopOver($PopInfoTitle = null, $PopInfoDesc = null) {

        if (!is_null($PopInfoTitle))
            return "<i class=\"popinfo icon-question-sign\" data-toggle=\"popover\" data-content=\"$PopInfoDesc\" data-original-title=\"$PopInfoTitle\"></i>";
    }

    /*
     * Este metodo fica responsavel por gerar todo o html, exibição de erros e etc dos inputs, textarea e selects
     */

    public static function ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle = null, $PopInfoDesc = null) {
        
        static::$attributes[$name] = $label;
        
        $class = ( count($errors) > 0 && $errors->first($name) ) ? 'has-error' : '';

        echo "<div class=\"form-group $class\">";
        
        if (!empty($label)){
            echo "<label for=\"$name\" class=\"control-label\">$label</label>";
        }
        
        echo self::PopOver($PopInfoTitle, $PopInfoDesc);
        echo $ContainerGroup;

        if ($class && static::$showMsg)
            echo "<span class=\"help-block text-danger\">{$errors->first($name)}</span>";
        echo '</div>';
    }

    public static function input($type, $name, $label, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null, $value = null) {
        $args = self::MergeArgs($args);
        $ContainerGroup = Form::input($type, $name, $value, $args);
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function inputText($name, $label, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null) {
        $args = self::MergeArgs($args);
        $ContainerGroup = Form::text($name, null, $args);
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }
    
    public static function inputCheckbox($name, $label, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null) {
        
        $args = self::MergeArgs($args);
        $ContainerGroup = Form::checkbox($name, 1, null, $args);
        
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function InputFile($name, $label, $errors, $PopInfoTitle = null, $PopInfoDesc = null) {
        $ContainerGroup = Form::file($name, null, array('class' => self::$clalsInput));
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function InputData($name, $label, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null) {
        $args['class']='datatimepicker';
        $args = self::MergeArgs($args);
        $ContainerGroup = '<div class="input-group date datatimepicker">';
        $ContainerGroup .= Form::text($name, null, $args);
        $ContainerGroup .= '<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span></div>';
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function inputTextarea($name, $label, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null) {
        $args = self::MergeArgs($args);
        $ContainerGroup = Form::textarea($name, null, $args);
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function inputSelect($name, $label, $data, $dafaul, $errors, $args = array(), $PopInfoTitle = null, $PopInfoDesc = null) {
        $defaul = ( Input::old($name) != "") ? Input::old($name) : $dafaul;
        $args = self::MergeArgs($args);
        $ContainerGroup = Form::select($name, $data, $defaul, $args);
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }
    
    public static function InputSelectMulti($name, $label, $data, $dafaul, $errors, $PopInfoTitle = null, $PopInfoDesc = null) {
        $defaul = ( Input::old($name) ) ? Input::old($name) : $dafaul;
        $args = self::MergeArgs( array('multiple') );
        $ContainerGroup = Form::select($name, $data, $defaul, $args);
        self::ControlGroup($ContainerGroup, $name, $label, $errors, $PopInfoTitle, $PopInfoDesc);
    }

    public static function MergeArgs($args) {
        $class  = self::$clalsInput . " ";
        $class .= (isset($args['class'])) ? $args['class'] : null;
        $arg = array('class' => $class);
        $args['class'] = $class;
        $args = array_merge($arg, $args);
        return $args;
    }
    
    /**
     * salva  names dos atributes em uma session::flash
     */
    public static function saveAttributes(){
        \Session::flash('name_attributes', static::$attributes);        
        return null;
    }
    /**
     * retorna  os atributes salvo na session::flash
     */
    public static function getAttributes($merge = array()){
        $data =  (array) \Session::get('name_attributes');        
        return array_merge($data, $merge);
    }

    public static function getGravatar($email, $img = false, $s = 64, $d = 'mm', $r = 'g', $atts = array('class' => 'media-object')) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    /**
     * Gera os botoes editar e excluir da tabela
     * 
     * Inicio do rota resorce
     * $strRoute = 'cms.tag'
     * ou
     * $strRoute = array('urledit', 'url action delete')
     */
    public static function groupButton($strRoute, $id = 0, $route_show =false) {

        if (is_array($strRoute)) {
            $urlEdit = $strRoute[0];
            $urlDele = $strRoute[1];
        } else {
            $urlEdit = URL::route("$strRoute.edit", $id);
            $urlDele = URL::route("$strRoute.destroy", $id);
        }
        
        if($route_show){
            $urlShow = URL::route("$strRoute.show", $id);
            echo '<td align="center">';
            echo '<a href="' . $urlShow . '" class="btn btn-default"> <i class="glyphicon glyphicon-search"></i>  </a>';
            echo '</td>';  
        }

        echo '<td align="center">';
        echo '<a href="' . $urlEdit . '" class="btn btn-info icon-edit bigger-150"></a>';
        echo '</td>';        
        echo '<td align="center">';
        echo Form::open(array('method' => 'DELETE', 'url' => $urlDele));
        echo '<button class="btn btn-danger btn-trash delete" type="submit"> <i class="icon-trash bigger-150"></i>  </button>';
        echo Form::close();
        echo '</td>';
    }
    
    /*
     * Retorna form com btn para deletar
     * $route pode ser passada a url da rota URL::route("midias.destroy", $id);
     */
    public static function ButtonDelete($route){
        echo '<td align="center">';
        echo Form::open(array('method' => 'DELETE', 'url' => $route));
        echo '<button class="btn btn-danger btn-trash" type="submit"> <i class="glyphicon glyphicon-trash"></i>  </button>';
        echo Form::close();
        echo '</td>';
    }

    /**
     * Gera os botoes Restaurar e remover da tabela
     */
    public static function groupButtonLixeira($urlRestoure, $urlDelete) {
        //cmsmidiadelete
        echo '<td align="center">';
        echo Form::open(array('method' => 'POST', 'url' => $urlRestoure));
        echo '<button class="btn btn-success" type="submit"> <i class="glyphicon glyphicon-refresh"></i>  </button>';
        echo Form::close();
        echo '</td>';
        echo '<td align="center">';
        echo Form::open(array('method' => 'DELETE', 'url' => $urlDelete));
        echo '<button class="btn btn-danger btn-trash-destroy" type="submit"> <i class="glyphicon glyphicon-trash"></i>  </button>';
        echo Form::close();
        echo '</td>';
    }

    public static function metaSummernote($nome, $label = null, $errors=array() ) {        
        $value = \VHelper::InputOld($nome);

        $class = ( count($errors) > 0 && $errors->first($nome) ) ? 'has-error text-danger' : '';
        
        $htm = ($label)? "<label for=\"$nome\" class=\"$class\"> $label </label>" : '';
        $htm .= "<textarea name=\"$nome\" style=\"display:none\">$nome</textarea>";
        $htm .= "<div class=\"meta-summernote\" data-name=\"$nome\" style=\"width: 100%;\">$value</div>";
        $htm .= ($class)? "<p class=\"has-error text-danger\">{$errors->first($nome)}</p>" : '';
        
        return $htm;
    }

    public static function summernote($nome) {
        $value = \VHelper::InputOld($nome);
        $htm = "<textarea name=\"$nome\" style=\"display:none\">$nome</textarea>";
        $htm .= "<div id=\"summernote-$nome\" style=\"width: 100%;\">$value</div>";
        return $htm;
    }

    /**
     * Este metodo resolve o problema do $model->all()->list('key','value');
     * Ele adiciona no array a cima uma opçao de valor padrao 
     */
    public static function selectDataPrepare($list, $default = ' -- Selecione -- ', $exclude = null) {
        $new = array('' => $default);
        
        if(count($list) < 1)return $new;
        
        foreach ($list as $key => $value)
            if ($key != $exclude)
                $new[$key] = $value;

        return $new;
    }

    /**
     * Retorna Html da tag img com atributos width e height
     * @param Midia $img
     * @param string $dimension
     * @param array $args
     * @return string
     */
    public static function img($img, $dimension = 'default', $args = array()) {
        $VImage = \App::make('App\Modules\Cms\Libraries\VImage');
        return $VImage->htmlimg($img, $dimension, $args);        
    }
    
    public static function thumb($img, $dimension = 'default', $args = array()) {
        $VImage = \App::make('App\Modules\Cms\Libraries\VImage');
        return $VImage->htmlThumb($img, $dimension, $args);        
    }

}