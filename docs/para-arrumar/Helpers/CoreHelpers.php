<?php
/**
 * Created by PhpStorm.
 * User: valmir
 * Date: 28/02/15
 * Time: 12:08
 */

class CoreHelpers {
    public static function dataToPT($date) {

        if (empty($date))
            return null;

        if (is_string($date))
            $date = DateTime::createFromFormat('Y-m-d', $date);

        return $date->format('d/m/Y');
    }

    public static function dataToEN($date) {

        if (empty($date) || strpos($date, '-'))
            return $date;

        if (is_string($date))
            $date = DateTime::createFromFormat('d/m/Y', $date);

        return $date->format('Y-m-d');
    }

    /**
     *  Função soma dias em uma data
     * 	@PARAM date-en /days numero de dias
     *  @RETURN string
     * */
    public static function addDayIntoDate($date, $days) {

        $novaData = explode("-", $date);
        $thisyear = $novaData[0];
        $thismonth = $novaData[1];
        $thisday = $novaData[2];

        $nextdate = mktime(0, 0, 0, $thismonth, $thisday + $days, $thisyear);

        return strftime("%Y-%m-%d", $nextdate);
    }

    public static function dataTimeToPT($date) {

        if (empty($date))
            return null;

        if (is_string($date))
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);

        return $date->format('d/m/Y H:i');
    }

    public static function dataString($date) {
        $date = strtotime($date);
        $Nmes = date('m', $date);
        $dia = date('d', $date);
        $hora = date('H:i', $date);

        $mes = static::mesExtenso($Nmes, true);
        $dataFormt = sprintf('%s %s %s', $dia, $mes, $hora);
        return $dataFormt;
    }

    /**
     * Retorna data no seguinte formato: 23 de abril de ,
     * @param string $date data-en
     * @return string
     */
    public static function dataExteno($date) {
        $date = strtotime($date);
        $Nmes = date('m', $date);
        $dia = date('d', $date);
        $ano = date('Y', $date);

        $mes = static::mesExtenso($Nmes);
        $dataFormt = sprintf('%s de %s de %s', $dia, $mes, $ano);
        return $dataFormt;
    }

    public static function dataTimeToEN($date) {

        if (is_string($date))
            $date = DateTime::createFromFormat('d/m/Y H:i', $date);

        return $date->format('Y-m-d H:i:s');
    }

    public static function formatValor($valor) {
        if (empty($valor) || $valor <= 0) {
            return 'R$ 0,00';
        }
        return "R$ " . number_format($valor, 2, ',', '.');
    }

    /**
     * Retorna por extenso o mes
     * caso passe true para $iniciais ele retornara apenas as 3 primeiras letras do mes e: Jan
     * @param int $mes
     * @param bol $iniciais
     * @return string
     */
    public static function mesExtenso($mes, $inicias = false) {
        $mes = (int) $mes;
        $meses = array('', "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

        $mesf = isset($meses[$mes]) ? $meses[$mes] : '';

        if ($inicias) {
            $mesf = substr($mesf, 0, 3);
        }

        return $mesf;
    }
}