# Composer.json
```json
"require": {
    ....
    "valmir/core" : "dev-master"
},
"repositories": [
    {
        "type": "git",
        "url": "git@bitbucket.org:valmirw3/lara-core.git"
    }
],
```
Ainda se preferir recomendamos algumas packagen para auxiliar no desenvolvimento. para utilizar inclua no seu requere-dev:
```json
"require-dev": {
    "way/generators": "~2.0",
    "fzaninotto/Faker": "1.2.*",
    "barryvdh/laravel-ide-helper": "dev-master",
    "itsgoingd/clockwork": "~1.3"
}
```
run: composer update

# App/config/app.php


```php
<?php
	'providers' => array(
		       'Valmir\Backend\BackendServiceProvider',//Meu Modulo Back-end
               'Valmir\Core\CoreServiceProvider',//Meu Modulo Core

                # DEV (opcional)
                'Way\Generators\GeneratorsServiceProvider', // Generators
                'Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider', // IDE Helpers
                'Clockwork\Support\Laravel\ClockworkServiceProvider', // DEBUG
	),	

	'aliases' => array(
                # DEV (opcional)
                'Clockwork' => 'Clockwork\Support\Laravel\Facade',
	),
```


#Model User
Adicionar o Entrust, caso for utilizar os filtros de permission
```php
<?php
class User extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait,
        \Zizaco\Entrust\HasRole;

```



#Terminal
##Migrations
```bash
php artisan migrate --package="valmir/core"
php artisan migrate
```
Na pasta de `docs/seeds` -> possui alguns [modelos de Seeds](files/seeds "") que podem ser copiados para dentro do seu projeto

##Config
```bash
php artisan config:publish valmir/core
```

### Routes ###
```php
<?php //routes.php

Route::group(array('prefix' => 'service/v1', 'namespace' => 'MyApp\Service', 'before' => "serviceToken", 'after' => 'cors'), function() 
{

    Route::resource('pais', 'PaisController');//ex

});    
```