clockworkConsole
=====================================

Extensão do clockwork, para exibir queries feitas pela api, através do terminal.


#Instal

Add isso no `start/artisan.php`

```php
if( App::environment() == "local") {
    //Monitora log do clockwork
    Artisan::add(new \Valmir\Core\Commands\clockworkConsole());
}
```