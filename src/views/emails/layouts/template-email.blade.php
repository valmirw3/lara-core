<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
    </head>
    <body>

    <center>
        <table style="width: 650px; font-family: verdana; color: #436DA6; font-size: 14px; border: 2px solid #ABC955; padding: 10px;">
            <tr>
                <td> <img src="{{ URL::asset('/assets/admin/images/logo.png') }}" width="100" height="49"/> </td>
                <td> 
                    <p style="text-align: right">{{ date('d/m/Y') }}  {{ date('H:i') }}</p>
                </td>
            </tr>
            <tr><td colspan="2"> &nbsp; </td></tr>
            <tr>
                <td colspan="2"> 
                    @yield('main')
                </td>
            </tr>
            <tr><td colspan="2"> &nbsp; </td></tr>
        </table>
    </center>

</body>
</html>

