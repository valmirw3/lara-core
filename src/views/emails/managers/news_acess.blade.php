@extends('Admin::emails.layouts.template-email')
@section('main')
    <h1>Novo acesso</h1>

    <p> Olá {{ $user->nome }}, </p>

    <p>
        Seja bem vindo ao sistema IAB.<br />
        Acesse o link a seguir para gerar uma nova senha.
    </p>
    <a href="{{ URL::route('admin.reset_password', array($token)) }}">   {{ URL::route('admin.reset_password', array($token)) }} </a>

    <p>Att</p>
@stop
<?php
    //myTodo melhorar texto email novo acesso
        //Colocar algo como seja bem vindo ao sistema IAB
?>
