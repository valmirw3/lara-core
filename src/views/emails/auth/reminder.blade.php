@extends('Admin::emails.layouts.template-email')
@section('main')
    <h1>Troca de senha</h1>

    <p> Olá {{ $user->nome }},</p>

    <p>Acesse o link a seguir para trocar a sua senha </p>
    <a href="{{ URL::route('admin.reset_password', array($token)) }}">   {{ URL::route('admin.reset_password', array($token)) }} </a>

    <p>Att</p>
@stop