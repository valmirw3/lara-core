<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Notificação de Log de error</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<h2> Erro {{ Config::get('core::core.mail_log.name_app') }} - {{date('d/m/Y H:m')}} </h2>
{{$mensagem}}
</body>
</html>