<?php
namespace Valmir\Core\Repository;


class UserLogsRepository{


    protected $table = 'user_logs';


    public function getLog($parans, $paginate = 50){

        $table = \DB::table( $this->table )
                        ->join('users', 'users.id', '=', "{$this->table}.user_id");


        if( is_array($parans) ) {
            $accept_keys = ['user_id', 'ip', 'model_id', 'model_table', 'tipo'];

            foreach ($parans as $k => $v) {
                if (!empty($v) && in_array($k, $accept_keys)) $table->where($k, $v);
            }
        }

        $data_ini = array_get($parans, 'data_ini');
        $data_fim = array_get($parans, 'data_fim');

        if( !empty($data_ini) ){
            $table->where("{$this->table}.created_at", '>=', $data_ini );
        }

        if( !empty($data_fim) ){
            $table->where("{$this->table}.created_at", '<=', $data_fim );
        }


        $table->select(["{$this->table}.*", 'users.nome as user'])
              ->orderBy('created_at', 'DESC');

        return $table->paginate($paginate)->appends( $parans );

    }



}