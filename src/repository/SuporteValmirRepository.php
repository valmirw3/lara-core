<?php
/**
 * Created by PhpStorm.
 * User: valmir
 * Date: 18/10/15
 * Time: 02:39
 */

namespace Valmir\Core\Repository;


class SuporteValmirRepository {

    protected $limit = 15;
    protected $type_limit = 'paginate';//take
    protected $orderby = "lastchange";
    protected $order = "ASC";
    protected $name_columns = [
        'id', 'trackid', 'name', 'email', 'category', 'priority', 'subject', 'message', 'dt',
        'lastchange', 'status', 'lastreplier', 'locked', 'archive', 'replierid'
    ];

    protected $category_id;

    /**
     * @var array
     */
    protected $paramns = array();


    protected $data_status = array(
        '0' => [
            'name'  => 'Novo',
            'color' => '#FF0000',
            'id'    => 0,
        ],
        '1' => [
            'name'  => 'Aguardando sua resposta',
            'color' => '#FF9933',
            'id'    => 1,
        ],
        '2' => [
            'name'  => 'Respondido pelo suporte',
            'color' => '#0000FF',
            'id'    => 2,
        ],
        '3' => [
            'name'  => 'Resolvido',
            'color' => '#008000',
            'id'    => 3,
        ],
        '4' => [
            'name'  => 'Em progresso',
            'color' => '#006400',
            'id'    => 4,
        ],
        '5' => [
            'name'  => 'Em espera',
            'color' => '#000000',
            'id'    => 5,
        ]
    );

    protected $data_priority = array(
        '0' => [
            'name'  => 'Crítico',
            'color' => '#9400d3',
            'id'    => 0,
        ],
        '1' => [
            'name'  => 'Alta',
            'color' => '#FF0000',
            'id'    => 1,
        ],
        '2' => [
            'name'  => 'Média',
            'color' => '#FF9900',
            'id'    => 2,
        ],
        '3' => [
            'name'  => 'Baixa',
            'color' => '#4a5571',
            'id'    => 3,
        ]
    );


    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;


    /**
     * ID da categoria em suporte.valmirbarbosa.com.br
     * @param int $categoryId
     */
    function __construct($categoryId)
    {
        $this->category_id = $categoryId;

        if( $this->category_id < 1){
            throw new \Exception('Ticket Valmir, invalid category id');
        }

        \Config::set('database.connections.suporte_valmir', [
            'driver'    => 'mysql',
            'host'      => 'valmirbarbosa.com.br',
            'database'  => 'valmi743_hesk',
            'username'  => 'valmi743_hesk_rd',
            'password'  => 'dA]STi%)R{Vg',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => 'hesk_',
        ]);

        $this->newQuery();
    }

    public function all()
    {
        $this->scopeCategoryId();
        $this->scopeOpenClient();

        if( $email = $this->getParam('email') ) {
            $this->query->where('email', $email);
        }

        if( $priority = $this->getParam('priority') ) {
            $this->query->where('priority', $priority);
        }

        if( $status = $this->getParam('status') ) {

            $status = explode(',', trim($status));
            $this->query->whereIn('status', $status);

        }else{
            $this->query->where('status', '<>', 3);
        }

        $this->scopeOrderby();
        $this->query->select($this->name_columns);


        $result = $this->query->get();
        return $this->collecion($result);
    }

    /**
     * @return $this
     */
    public function newQuery()
    {
        $this->query = \DB::connection('suporte_valmir')->table('tickets');
        return $this;
    }

    /**
     * @param array $paramns
     */
    public function setParamns($paramns)
    {
        $this->paramns = $paramns;
    }

    public function scopeCategoryId()
    {
        $this->query->where('category', $this->category_id);
    }


    public function getParam($key = null, $defaul = null){
        if(is_null($key)) return $this->paramns;
        return array_get($this->paramns, $key, $defaul);
    }

    /**
     * Informa a quantidade de registro por consulta
     * @param int $perpage
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @param string $orderby
     */
    public function setOrderby($orderby, $order = "ASC")
    {
        $order = strtoupper($order);
        $this->orderby = (in_array($orderby, $this->name_columns) )? $orderby : $this->orderby;
        $this->order = ( in_array($order, ['ASC', "DESC"]) )? $order : $this->order;
    }

    /**
     * Verifica se será 'paginate' para paginação ou 'take' para busca simples
     * @param string $type_limit  paginate|take
     */
    public function setTypeLimit($type_limit)
    {
        $this->type_limit = $type_limit;
    }

    /**
     * Busca registro pelo id
     * @param int $id
     * @return \Illuminate\Support\Collection|static|null
     */
    public function find($id)
    {
        return $this->newQuery()->find($id);
    }

    /**
     * Busca registro pelo id, caso não encontre, gera uma exception
     * @param int $id
     * @return \Illuminate\Support\Collection|static|null
     */
    public function findOrfail($id)
    {
        return $this->newQuery()->findOrFail($id);
    }

    protected function scopeOrderby()
    {        
        return $this->query->orderBy($this->orderby, $this->order);
    }


    /**
     * Filtra por tickets que ainda estão visivel para cliente
     * @return mixed
     */
    protected function scopeOpenClient()
    {
        return $this->query->where('locked', 0)->where('archive', 0);
    }

    protected function scopeAbort()
    {
        return $this->query->where('id', 0);
    }

    protected function collecion($collection)
    {
        $result = [];

        foreach($collection as $row)
        {
            array_push($result, $this->transformer($row));
        }

        return $result;
    }

    protected function transformer($row)
    {
        $data = [
            'id' => $row->id,
            'trackid' => $row->trackid,
            'name' => $row->name,
            'email' => $row->email,
            'category' => $row->category,
            'priority' => array_get($this->data_priority, $row->priority),
            'subject' => $row->subject,
            'message' => $row->message,
            'status' => array_get($this->data_status, $row->status),
            //'history'=> "<ul>$row->history</ul>",
            'lastreplier' => $row->lastreplier,
            'replierid' => $row->replierid,
            'archive' => $row->archive,
            'locked' => $row->locked,
            'link' => 'http://suporte.valmirbarbosa.com.br/ticket.php?track=' . $row->trackid . '&e=' . $row->email,
            'created_at' => $row->dt,
            'updated_at' => $row->lastchange,
        ];

        return $data;
    }

    /**
     * @return array
     */
    public function getDataStatus()
    {
        return $this->data_status;
    }

    /**
     * @return array
     */
    public function getDataPriority()
    {
        return $this->data_priority;
    }

}