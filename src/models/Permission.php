<?php

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{

    protected $fillable = ['name', 'display_name', 'group_name'];

    /**
     * Ardent validation rules.
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required|between:3,128|unique:permissions',
        'display_name' => 'required|between:3,128',
        'group_name' => 'required|between:3,128',
    );

    public static $customMessages = array(
        'name.required' => 'É obrigatória a indicação de um valor para o campo nome.',
        'display_name.required' => 'É obrigatória a indicação de um valor para o campo descrição.',
        'group_name.required' => 'É obrigatória a indicação de um valor para o campo grupo.',
    );

}