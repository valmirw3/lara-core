<?php

use Zizaco\Entrust\EntrustRole;


class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name'];

    /**
     * Ardent validation rules.
     *
     * @var array
     */
    public static $rules = array(
        'name' => 'required|between:3,128|unique:roles',
        'display_name' => 'required|between:3,228',
    );

    public static $customMessages = array(
        'name.required' => 'É obrigatória a indicação de um valor para o campo nome.',
        'display_name.required' => 'É obrigatória a indicação de um valor para o campo descrição.',
    );

}