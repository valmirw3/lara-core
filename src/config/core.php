<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Authentication - Register
	|--------------------------------------------------------------------------
	|
	| Ativa rota service para registrar externo
	| 
	| Supported: true, false
	|
	*/

	'route_register_public' => false,
    
	/*
	|--------------------------------------------------------------------------
	| LogUsers - 
	|--------------------------------------------------------------------------
	|
	| Ativa log de todas as queryes executadas no banco feita pelos usuários
	| 
	| Supported: true, false
	|
	*/

	'enable_log' => true,

	/*
	|--------------------------------------------------------------------------
	| Auth HMAC -
	|--------------------------------------------------------------------------
	|
	| Ativa authenticação via HMAC para aplicativos mobile
	|
	|
	*/

    'auth_hmac' => array(
        'enable_auth' => false,
        'onlyAuthToken' => false, // validar apenas se o token existe na tabela user
        'private_key' => null
    ),


    /*
    |--------------------------------------------------------------------------
    | E-MAIL ATENDIMENTO
    |--------------------------------------------------------------------------
    |
    | Sempre que for apresentado uma msg de erro, ou enviar alguma notificação
    | para os donos do sistema, utilizar o email a baixo.
    |
    |
    */

    'email_atendimento' => 'contato@valmirbarbosa.com.br',

	/*
	|--------------------------------------------------------------------------
	| VIEWS ERRORS
	|--------------------------------------------------------------------------
	|
	| Permite configurar e personar as telas de errors do sistema
	|
	| 403 => Sem Permissão de acesso
	| 404 => Not Found
	| 500 => Erro interno do servidor
	|
	*/

    'view_errors' => array(
        '403' => 'core::errors.403',
        '404' => 'core::errors.404',
        'error' => 'core::errors.error',
        'tokenException' => 'core::errors.tokenException',
    ),

    /*
    |--------------------------------------------------------------------------
    | Monitoramento de errors via E-mail
    |--------------------------------------------------------------------------
    |
    | Envia e-mails de erros que acontencer na aplicação
    |
    |
    */

    'mail_log' => array(
        'send_error' => true,
        'name_app'   => 'Nome não definido',
        'email_dev_1' => 'suporte@valmirbarbosa.com.br',
        'email_dev_2' => null,
    ),


	
);
