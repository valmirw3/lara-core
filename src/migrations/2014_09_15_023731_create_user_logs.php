<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->string('ip')->nullable();
			$table->integer('model_id')->nullable();
			$table->string('model_table')->nullable();
			$table->text('mesage')->nullable();
			$table->string('bindings')->nullable();
			$table->string('tipo');//INSERT, UPDATE, DELETE
			$table->string('query');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_logs');
	}

}
