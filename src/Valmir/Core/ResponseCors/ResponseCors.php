<?php

namespace Valmir\Core\ResponseCors;

use Response;

class ResponseCors {

    public function responseRouteOptions() {
        $response = Response::make('ok', 200);

        $origen = (isset($_SERVER['HTTP_ORIGIN'])) ? $_SERVER['HTTP_ORIGIN'] : '*';

        $response->headers->set('Access-Control-Allow-Origin', "{$origen}");
        $response->headers->set('Access-Control-Allow-Methods','POST, GET, OPTIONS, PUT, DELETE, PATCH');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, Origin, Authorization, Content-Type, accept, ng-csrf-token, X-HASH, X-MICROTIME, X-PUBLIC-KEY, X-UNIDADE, MODULE');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        return $response;
    }

    public function make($message, $code, $status = false) {

        $status = ($status) ? 'success' : 'error';
        $content = (is_array($message))? json_encode($message) : json_encode(['message' => $message]);
        //$origen = array_get($_SERVER, 'HTTP_ORIGIN', '*');
        $origen = (isset($_SERVER['HTTP_ORIGIN'])) ? $_SERVER['HTTP_ORIGIN'] : '*';

        $headers = array(
            //'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
           // 'Access-Control-Allow-Headers' => 'X-Requested-With, Origin, Authorization, Cache-Control, Content-Type, ng-csrf-token',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Allow-Origin' => "{$origen}",
            'Content-Type' => 'application/json'
        );

        $response = Response::make($content, $code, $headers);
        
        return $response;
    }

}
