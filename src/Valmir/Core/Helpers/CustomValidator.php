<?php

namespace Valmir\Core\Helpers;



class CustomValidator extends \Illuminate\Validation\Validator  {


    public function __construct($translator, $data, $rules, $messages = array(), $customAttributes = array())
    {
        parent::__construct($translator, $data, $rules, $messages, $customAttributes);
        $langValidator = (array) trans('core::validation');
        $this->setCustomMessages( $langValidator );
    }

    /**
     * Validates CNPJ (Cadastro Nacional da Pessoa Jurídica)
     *
     * CNPJ is the Brazilian corporate taxpayer identification number.
     *
     * @param   string $cnpj CNPJ number to validate
     * @return  bool TRUE if the number is a valid CNPJ, FALSE otherwise
     * @access  public
     * @since   20050619
     */
    public function validateCnpj($attribute, $value, $parameters) {
        // Canonicalize input
        $cnpj = sprintf('%014s', preg_replace('{\D}', '', $value));

        // Validate length and CNPJ order
        if ((strlen($cnpj) != 14) || (intval(substr($cnpj, -4)) == 0)) {
            return false;
        }

        // Validate check digits using a modulus 11 algorithm
        for ($t = 11; $t < 13;) {
            for ($d = 0, $p = 2, $c = $t; $c >= 0; $c--, ($p < 9) ? $p++ : $p = 2) {
                $d += $cnpj[$c] * $p;
            }

            if ($cnpj[++$t] != ($d = ((10 * $d) % 11) % 10)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validates CPF (Cadastro de Pessoas Físicas)
     *
     * CPF is the Brazilian individual taxpayer identification number.
     *
     * @param   string $cpf CPF number to validate
     * @return  bool TRUE if the number is a valid CPF, FALSE otherwise
     * @access  public
     * @since   20050617
     */
    public function validateCpf($attribute, $value, $parameters) {
        // Canonicalize input
        $cpf = sprintf('%011s', preg_replace('{\D}', '', $value));

        // Validate length and invalid numbers
        if ((strlen($cpf) != 11) || ($cpf == '00000000000') || ($cpf == '99999999999')) {
            return false;
        }

        // Validate check digits using a modulus 11 algorithm
        for ($t = 8; $t < 10;) {
            for ($d = 0, $p = 2, $c = $t; $c >= 0; $c--, $p++) {
                $d += $cpf[$c] * $p;
            }

            if ($cpf[++$t] != ($d = ((10 * $d) % 11) % 10)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Verifica se o texto possui apenas letras e espaçoes
     * @return boolean
     */
    public function validateAlphaString($attribute, $valor, $parameters) {

        if(preg_match('/^[^0-9[:punct:]]+$/', $valor)) {
            return true;
        }

        return false;
    }


    public function validateDataIdade($attribute, $valor, $parameters){
        $ano = (int) substr($valor, 0, 4);

        $ano_atual = (int) date('Y');

        $dif = $ano_atual - $ano;

        if( $dif > 110 ){
            return false;
        }

        return true;
    }

    public function validateUniqueCidade($attribute, $valor, $parameters){
        $data = $this->getData();
        $estado_id = array_get($data, 'estado_id');
        $cidade = \Cidade::where('estado_id', $estado_id)->where('cidade', $valor)->first();
        return ( is_null($cidade) )? true : false;
    }


}