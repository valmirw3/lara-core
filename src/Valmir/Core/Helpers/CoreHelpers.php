<?php
namespace Valmir\Core\Helpers;

use DateTime;
class CoreHelpers {


    public function dataToPT($date) {

        if ( empty($date) || $date == "0000-00-00")
            return null;

        if (is_string($date))
            $date = DateTime::createFromFormat('Y-m-d', $date);

        return $date->format('d/m/Y');
    }

    public function dataToEN($date) {

        if (empty($date) || strpos($date, '-'))
            return $date;

        if (is_string($date))
            $date = DateTime::createFromFormat('d/m/Y', $date);

        return $date->format('Y-m-d');
    }

    /**
     *  Função soma dias em uma data
     * 	@PARAM date-en /days numero de dias
     *  @RETURN string
     * */
    public function addDayIntoDate($date, $days) {

        $novaData = explode("-", $date);
        $thisyear = $novaData[0];
        $thismonth = $novaData[1];
        $thisday = $novaData[2];

        $nextdate = mktime(0, 0, 0, $thismonth, $thisday + $days, $thisyear);

        return strftime("%Y-%m-%d", $nextdate);
    }

    public function dataTimeToPT($date) {

        if (empty($date))
            return $date;

        if (is_string($date))
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);

        return $date->format('d/m/Y H:i');
    }

    public function dataString($date) {
        $date = strtotime($date);
        $Nmes = date('m', $date);
        $dia = date('d', $date);
        $hora = date('H:i', $date);

        $mes = $this->mesExtenso($Nmes, true);
        $dataFormt = sprintf('%s %s %s', $dia, $mes, $hora);
        return $dataFormt;
    }

    /**
     * Retorna data no seguinte formato: 23 de abril de ,
     * @param string $date data-en
     * @return string
     */
    public function dataExteno($date) {
        $date = strtotime($date);
        $Nmes = date('m', $date);
        $dia = date('d', $date);
        $ano = date('Y', $date);

        $mes = $this->mesExtenso($Nmes);
        $dataFormt = sprintf('%s de %s de %s', $dia, $mes, $ano);
        return $dataFormt;
    }

    public function dataTimeToEN($date) {

        if (is_string($date))
            $date = DateTime::createFromFormat('d/m/Y H:i', $date);

        return $date->format('Y-m-d H:i:s');
    }

    public function formatValor($valor) {
        if (empty($valor) || $valor <= 0) {
            return 'R$ 0,00';
        }
        return "R$ " . number_format($valor, 2, ',', '.');
    }

    /**
     * Retorna por extenso o mes
     * caso passe true para $iniciais ele retornara apenas as 3 primeiras letras do mes e: Jan
     * @param int $mes
     * @param bol $iniciais
     * @return string
     */
    public function mesExtenso($mes, $inicias = false) {
        $mes = (int) $mes;
        $meses = array('', "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

        $mesf = isset($meses[$mes]) ? $meses[$mes] : '';

        if ($inicias) {
            $mesf = substr($mesf, 0, 3);
        }

        return $mesf;
    }


    /**
     * Utilizando em classes de log de messages e notificações
     * @param array|string $msg
     * @return string
     */
    public function trataMsg($msg)
    {

        if(is_array($msg)){
            $texto = $msg[0];
            unset($msg[0]);

            foreach ($msg as $key=>$v)
            {

                if(is_array($v)){

                    foreach ($v as $k2=>$v2){
                        if(!is_array($v2))
                        $texto = str_ireplace(":$k2", $v2, $texto);
                    }

                }else{
                    $texto = str_ireplace(":$key", $v, $texto);
                }

            }

            $msg = $texto;
        }

        return $msg;
    }
}