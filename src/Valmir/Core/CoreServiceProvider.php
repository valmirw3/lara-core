<?php

namespace Valmir\Core;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Valmir\Core\Helpers\CustomValidator;

class CoreServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {

        $this->package('valmir/core', 'core');

        $this->app->bind('\Valmir\Core\Support\Contracts\AuthLoginInterface', '\Valmir\Core\Auth\AuthLogin');

        $this->app->singleton('response_cors', '\Valmir\Core\ResponseCors\ResponseCors');
        $this->app->singleton('core_role', '\Valmir\Core\Roles\Roles');
        $this->app->singleton('core_helpers', '\Valmir\Core\Helpers\CoreHelpers');
        $this->app->singleton('link_token', 'Valmir\Core\LinkToken\LinkToken');

        $this->setupLog();
        $this->setupAuthHMAC();
        $this->setupCustomValidator();

        include __DIR__ . '/../../filters-core.php';
        include __DIR__ . '/../../routes-core.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDependencies();
        $this->registerAlias();
    }

    /**
     * Providers
     * @return void
     */
    public function registerDependencies()
    {
        $this->app->register('Zizaco\Entrust\EntrustServiceProvider');
    }

    /**
     * Facades
     * @return void
     */
    public function registerAlias()
    {
         $alias = AliasLoader::getInstance();
         $alias->alias('ResponseCors', '\Valmir\Core\Support\Facades\ResponseCorsFacade');
         $alias->alias('CoreRole', '\Valmir\Core\Support\Facades\RolesFacade');
         $alias->alias('CoreHelpers', '\Valmir\Core\Support\Facades\CoreHelpersFacade');
         $alias->alias('Entrust', 'Zizaco\Entrust\EntrustFacade');
    }

    public function setupCustomValidator()
    {

        \Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new \Valmir\Core\Helpers\CustomValidator($translator, $data, $rules, $messages);
        });

    }
    
    public function setupLog()
    {

        if( $this->app['config']->get('core::core.enable_log') !== true ){
            return;
        }

        // Bring the application container instance into the local scope so we can
        // import it into the filters scope.
        $app = $this->app;

        $this->app['log_user'] = $this->app->share(function($app) {
            return new \Valmir\Core\LogUsers\LogUsers($app['db']->connection());
        });
        
        $this->app->finish(function() use ($app) {
           // $app['log_user']->registerAllLog();
            $app['log_user']->saveLog();
        });
        
        
        \Event::listen('illuminate.query', function($query, $bindings, $time, $name) {
            $id = \DB::getPdo()->lastInsertId();
            \App::make('log_user')->registerLog($query, $bindings, $id); 
        });
    }

    public function setupAuthHMAC()
    {

        if( $this->app['config']->get('core::core.auth_hmac.enable_auth') !== true ){
            return;
        }

        // Bring the application container instance into the local scope so we can
        // import it into the filters scope.
        $app = $this->app;

        $this->app['auth_hmac'] = $this->app->share(function($app) {
            $config = $app['config']->get('core::core.auth_hmac');
            return new \Valmir\Core\Auth\AuthHMAC($config);
        });

        \Route::filter('core.auth.hmac', function() use($app) {

            if( ! $app['auth_hmac']->authenticate($app['request']) ){
                return $app['auth_hmac']->getResponseError();
            }

        });

        \Route::filter('core.auth.hmac_public', function() use($app) {

            if( ! $app['auth_hmac']->authenticatePublic($app['request']) ){
                return $app['auth_hmac']->getResponseError();
            }

        });


    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}
