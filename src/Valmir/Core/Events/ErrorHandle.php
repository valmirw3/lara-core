<?php

namespace Valmir\Core\Events;

use Request;
use Config;
use Mail;

/**
 * Class ErrorHandle
 * @package Valmir\Core\Events
 *
 *
 *
 *
'mail_log' => array(
'send_error' => true,
'Name app'   => 'Nome não definido',
'' => array(
'suporte@valmirbarbosa.com.br'
)
),
 */

class ErrorHandle {
    
    /**
     * @var \Exception 
     */
    protected $exception;
    
    protected $ignore_http_code = [404];



    public function send(  $exception, $code){
        
        if($this->checkSend($code))
        {

                $this->exception = $exception;
                $data = ['mensagem' => $this->getMensagem()];

                Mail::send('core::emails.notification-error', $data, function($message)
                {
                    $subject = sprintf('Error - %s [%s]', Config::get('core::core.mail_log.name_app'), \App::environment());
                    $message->to( Config::get('core::core.mail_log.email_dev_1') )->subject($subject);

                    if($mail = Config::get('core::core.mail_log.email_dev_2'))
                        $message->cc( $mail );
                });
        
        }
        
    }
    
    
    
    protected function checkSend($code){

        if( 
                Config::get('app.debug') === false &&
                Config::get('core::core.mail_log.send_error') === true &&
                in_array($code, $this->ignore_http_code) === false 
          ){
            return true;
          }else{
              return false;
          }
        
        
    }


    protected function getMensagem( ){
        $mensagem = $this->getInfoUser() .
                    $this->getInfoRequest() .
                    $this->getInfoInput().
                    $this->getContext();
                
        return $mensagem;
    }
    
    
    protected function getContext(){
        $info = '<hr />';
        $info .= "<p>";
        $info .= "<b> ERROR: </b> <br /> ";
        $info .= "<pre>{$this->exception}</pre>";
        $info .= "</p>";
        return $info;
    }
    
    
    protected function getInfoUser(){
        
        $info = '<hr />';
        
        if( \Auth::guest() ){
            
            $info .= "<p> <b> Dados do Usuário: </b> <br /> Não logado </p>";
            
        }else{
        
            $user  = \Auth::user();
            $info .= "<p>";
            $info .= "<b> Dados do Usuário: </b> <br /> ";
            $info .= "E-mail: {$user->email} <br />";
            $info .= "User_ID: {$user->id} <br />";
            $info .= "</p>";
            
        }
        
        return $info;
    }
    
    protected function getInfoInput(){
            $info = '<hr />';
            $input = \Input::all();
            
            $info .= "<p>";
            $info .= "<b> Inputs: </b> <br /> ";
            
            foreach($input as $key=>$value){
                $info .= "{$key}: {$value} <br />";
            }
            
            $info .= "</p>";
            
            return $info;
    }
    
    protected function getInfoRequest(){
            $info = '<hr />';
            $info .= "<p>";
            $info .= "<b> Dados da requisição: </b>";
            $info .= "<br />URL: "      . Request::getUri();
            $info .= "<br />IP: "       . Request::getClientIp();
            $info .= "<br />METHOD: "   . Request::getMethod();
            $info .= "</p>";
            return $info;
    }
    
    
}
