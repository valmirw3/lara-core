<?php
namespace Valmir\Core\Events;

class SubscribeHandler {

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        $events->listen('core::error.send', 'Valmir\Core\Events\ErrorHandle@send');
    }

}