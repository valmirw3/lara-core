<?php

namespace Valmir\Core\LogUsers;

use Illuminate\Database\Connection;

class LogUsers {

    protected $connection;
    protected $query_register = array();
    protected $var_base_log;
    protected $ignore_tables = ['user_logs'];

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection) {
        $this->connection = $connection;
    }

    
    public function registerAllLog() {

        $logs = $this->connection->getQueryLog();

        foreach ($logs as $q) {
            $query = array_get($q, 'query');
            $bindings = array_get($q, 'bindings');
            $this->registerLog($query, $bindings);
        }

        $this->saveLog();
    }

    public function registerLog($query, $bindings, $id = null) {
        $sql = $this->createRunnableQuery($query, $bindings);
        $sql = str_ireplace("`", '', $sql);

        if (preg_match('/insert|update|delete/', $query, $math)) {
            switch ($math[0]) {
                case 'update':
                    $this->setQueryUpdate($sql);
                    break;
                case 'insert':
                    $this->setQueryInsert($sql, $id);
                    break;
                case 'delete':
                    $this->setQueryDelete($sql);
                    break;
            }
        }
        
    }

    public function saveLog() {
        if (count($this->query_register)) {
            $this->connection->table('user_logs')->insert($this->query_register);
        }
    }

    protected function setQueryInsert($sql, $id = null) {

        if (preg_match("/^INSERT into (.*?) \((.*?)\) values \((.*?)\)$/", $sql, $math)) {
            $tabela = $math[1];
            $t1 = explode(',', $math[2]);
            $t2 = explode(',', str_replace("'", '', $math[3]));
            
            $parans = array();
            foreach ($t1 as $key=>$k){
                $parans[$k] = array_get($t2, $key);
            }
            
            $bindings = json_encode($parans);
            $this->setQuery($id, $tabela, $sql, $bindings, 'Criou o registro', 'INSERT');
        }
    }

    protected function setQueryDelete($sql) {
        
        if (preg_match("/^DELETE FROM (.*?) WHERE (.*?)$/", $sql, $math)) {
            $where = json_decode($this->paransToJson($math[2]), true);
            $id = (int) array_get($where, 'id');
            $bindings = $this->paransToJson($math[2]);
            $this->setQuery($id, $math[1], $sql, $bindings, 'Excluiu o registro', 'DELETE');
        }
        
    }
    
    protected function setQueryUpdate($sql) {

        if (preg_match("/^UPDATE (.*?) set (.*?) WHERE (.*?)$/", $sql, $math)) {

            $where = json_decode($this->paransToJson($math[3]), true);
            $id = (int) array_get($where, 'id');

            $bindings = $this->paransToJson($math[2]);
            $this->setQuery($id, $math[1], $sql, $bindings, 'Atualizou o registro', 'UPDATE');
        }
    }

    protected function paransToJson($str) {
        $str = str_ireplace("'", '', $str);
        $d = explode(',', $str);

        $new = array();
        foreach ($d as $s) {
            $v = explode('=', $s);
            $key = trim(array_get($v, 0));
            if(!empty($key)) {
                $new[$key] = trim(array_get($v, 1));
            }
        }

        return json_encode($new);
    }

    protected function setQuery($id, $table, $sql, $bindings, $mesage, $tipo) {

        if (in_array($table, $this->ignore_tables)) {
            return null;
        }

        if (is_null($this->var_base_log)) {
            $user = ( \Auth::check() ) ? \Auth::user()->id : null;
            $this->var_base_log = array(
                'user_id' => $user,
                'ip' => \Request::getClientIp(),
                'created_at' => new \Carbon\Carbon(),
            );
        }

        $log = array(
            'model_id' => $id,
            'model_table' => $table,
            'mesage' => $mesage,
            'bindings' => json_encode($bindings),
            'query' => $sql,
            'tipo' => $tipo,
        );

        $this->query_register[] = array_merge($this->var_base_log, $log);
    }

    protected function createRunnableQuery($query, $bindings) {
        # add bindings to query
        $bindings = $this->connection->prepareBindings($bindings);

        foreach ($bindings as $binding) {
            $binding = $this->connection->getPdo()->quote($binding);

            $query = preg_replace('/\?/', $binding, $query, 1);
        }

        # highlight keywords
        $keywords = array('select', 'insert', 'update', 'delete', 'where', 'from', 'limit', 'is', 'null', 'having', 'group by', 'order by', 'asc', 'desc');
        $regexp = '/\b' . implode('\b|\b', $keywords) . '\b/i';

        $query = preg_replace_callback($regexp, function($match) {
            return strtoupper($match[0]);
        }, $query);

        return $query;
    }

    /**
     * $log = \DB::getQueryLog();
      $this->connection = \DB::
      $query = array_get($log, '2.query');
      $bindings = array_get($log, '2.bindings');
      dd($log, $this->createRunnableQuery($query, $bindings));
     */
}
