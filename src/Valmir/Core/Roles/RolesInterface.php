<?php

namespace Valmir\Core\Roles;

interface RolesInterface{

    public function can($can);

    public function hasRole($role);

}