<?php


namespace Valmir\Core\Roles;


class Roles implements RolesInterface{

    public function can($can)
    {
        return \Entrust::can($can);
    }

    public function hasRole($role)
    {
        return \Entrust::hasRole($role);
    }

    public static function canOrFail($can){

        if( ! \Entrust::can($can) ){
            \App::abort(403, "Não autorizado, solicite a permissão ($can) para seu webmaster.");
        }

    }

}