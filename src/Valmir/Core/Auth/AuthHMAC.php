<?php
/**
 * Created by PhpStorm.
 * User: valmir
 * Date: 11/03/15
 * Time: 03:25
 */

namespace Valmir\Core\Auth;


class AuthHMAC {

    const CODE_EMPTY_PUBLIC_KEY = "EMPTY_KEY";
    const CODE_EMPTY_TIME       = "EMPTY_TIME";
    const CODE_EMPTY_HASH       = "EMPTY_HASH";
    const CODE_EXPIRED_TIME     = "EXPIRED_TIME";
    const CODE_NOT_MATCH        = "NOT_MATCH";
    const CODE_USER_NOT_FOUND   = "USER_NOT_FOUND";
    const CODE_LOGIN_FAILED     = "LOGIN_FAILED";

    const TIME_EXPIRATION       = 180;//180 min = 3horas

    /**
     * @var string
     */
    protected $privateKey;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var array
     */
    protected $errorResponse;

    protected $environment;

    function __construct( $config)
    {
        $this->config = (array) $config;
        $this->privateKey = array_get($this->config, 'private_key');
    }

    /**
     * Check Timestamp
     *
     * Uses the header value timestamp to check against the current timestamp
     * If the request was made within a reasonable amount of time (40 min),
     */
    public function check_timestamp($clientMicrotime) {

        if(empty($clientMicrotime)) {
            $this->setError(self::CODE_EMPTY_TIME, 'Not defined time', 405);
            return false;
        }

        $serverMicrotime = microtime(true);
        $timeDiff = $serverMicrotime - $clientMicrotime;

        $segundos = self::TIME_EXPIRATION * 60;

        if($timeDiff > $segundos){
            $this->setError(self::CODE_EXPIRED_TIME, 'Atualize a hora do seu sistema', 405);
            return false;
        }

        return true;
    }

    /**
     * Authenticate
     *
     * This is the authenticate method where we check the hash from the client against
     * a hash that we will recreate here on the sevrer. If the 2 match, it's a pass.
     *
     * @param \Request $request
     * @return bool
     */
    public function authenticate($request) {

        $xHash     = $request->header('X-HASH');
        $pubicKey  = $request->header('X-PUBLIC-KEY', $request->get('acess_token'));
        $timestamp = $request->header('X-MICROTIME');

        if( !empty($pubicKey) && array_get($this->config, 'onlyAuthToken') === true  ){
            return $this->authUser($pubicKey);
        }

        if( empty($pubicKey) ){
            $this->setError(self::CODE_EMPTY_PUBLIC_KEY, null, 405);
            return false;
        }elseif(empty($xHash)){
            $this->setError(self::CODE_EMPTY_HASH, 'Not defined hash', 405);
            return false;
        }else if( ! $this->check_timestamp($timestamp) ){
            return false;
        }

        $hash = $this->create_hash($pubicKey, $timestamp);

        if($xHash !== $hash) {
            $this->setError(self::CODE_NOT_MATCH, 'Not macth token', 401);
            return false;
        }

        return $this->authUser($pubicKey);
    }

    /**
     * Authenticate Public
     *
     * Verifica se o client tem os token HMAC
     * Esse methodo não loga um user, apenas compara as chave token
     *
     * @param \Request $request
     * @return bool
     */
    public function authenticatePublic($request) {

        $xHash     = $request->header('X-HASH');
        $pubicKey  = $request->header('X-PUBLIC-KEY');

        if( empty($pubicKey) ){
            $this->setError(self::CODE_EMPTY_PUBLIC_KEY, null, 405);
            return false;
        }elseif(empty($xHash)){
            $this->setError(self::CODE_EMPTY_HASH, 'Not defined hash', 405);
            return false;
        }

        $hash = $this->create_hash($pubicKey, null);

        if($xHash !== $hash) {
            $this->setError(self::CODE_NOT_MATCH, 'Not macth token', 401);
            return false;
        }

        return true;
    }

    /**
     * Create Hash
     *
     * This method is where we'll recreate the hash coming from the client using the secret key to authenticate the
     * request
     */
    public function create_hash($message, $timestamp)
    {
        return hash_hmac('sha1', $message.$timestamp, $this->privateKey);
    }


    public function setError($errorCode, $message = "Efetue o login novamente", $statuCode = 401)
    {
        $this->errorResponse = [
            'code' => $errorCode,
            'http_code' => $statuCode,
            'message' => $message,
            'validation' => array()
        ];
    }

    public function getResponseError()
    {
        $response = \Response::make(json_encode($this->errorResponse), 401);
        $response->header('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Consulta user, e loga ele
     * @param string $token
     * @return bool
     */
    private function authUser($token){
        $user = \User::where('api_token', $token)->first();

        if(is_null($user)){
            $this->setError(self::CODE_USER_NOT_FOUND);
            return false;
        }

        if( ! \Auth::onceUsingId($user->id) ){
            $this->setError(self::CODE_LOGIN_FAILED, "Falha ao efetuar login");
            return false;
        }

        return true;
    }

}