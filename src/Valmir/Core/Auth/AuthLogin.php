<?php namespace Valmir\Core\Auth;

use User;
use Auth;
use Password;
use \League\Fractal\Resource\Item;
use Valmir\Core\Support\Contracts\AuthLoginInterface;
use \Valmir\Core\Transformer\UserTransformer;

class AuthLogin implements AuthLoginInterface {

    /**
     * @var \Illuminate\Support\MessageBag 
     */
    protected $error;
    
    protected $userRegistrado;
    
    protected $userAuth;

    protected $frase;


    public function login( $credentials, $remember = true) {

        //myTodo criar opção no config para checar se é obrigatorio confirmar email antes de efetuar login
        $credentials = array_only($credentials, ['email', 'password']);

        if( Auth::attempt($credentials, $remember) ){
            $this->setUserAuth(Auth::user());
            return true;
        }

        $this->frase = 'E-mail ou senha inválido!';
            
        return false;
    }


    /**
     * @param \Illuminate\Auth\UserInterface $user
     * @return bool
     */
    public function loginUser($user, $remember = true) {

        if(empty($user) ){
            return false;
        }

        Auth::login($user,$remember);
        $this->setUserAuth(Auth::user());
        return true;
    }

    public function logout() {
        $this->frase = 'Sessão encerrada';
        Auth::logout();
    }

    public function registrar($data) {

        if (!$this->validate($data)) {
            return false;
        }
        
        $data['password'] = \Hash::make($data['password']);
        
        $this->userRegistrado = new User;
        $this->userRegistrado->fill($data);
        return $this->userRegistrado->save();            
    }

    public function update($id, $data) {

        $this->userRegistrado = User::find($id);

        if( is_null($this->userRegistrado) ){
            $this->frase = "User não encontrado";
            return false;
        }


        $pwd = array_get($data ,'password');
            //myTodo melhorar essa validação de senhas
        if( empty($pwd) ) {
            unset(User::$rules['password']);
            unset(User::$rules['password_confirmation']);
        }else if( array_get($data, 'password_confirmation') !== $pwd ) {
            $this->frase = "Senhas não coincidem.";
            return false;
        }else{
            $pwd = \Hash::make($pwd);
            $data['password'] = $pwd ;
            unset( User::$rules['password'] );
            unset( User::$rules['password_confirmation'] );
        }


        $this->userRegistrado->fill($data);

        if ( !$this->userRegistrado->update() ) {
            $this->error = $this->userRegistrado->errors();
            return false;
        }


        return true;
    }



    public function validate($data) {
        $rules = User::$rules;        
        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            $this->error = $validator->errors();
            return false;
        }

        return true;
    }

    public function getErroValidate() {
        return $this->error;
    }

    public function getUserAuth() {
        
        if(empty($this->userAuth)){
            $this->userAuth = \Auth::user();
        }
        
        return $this->userAuth;
    }

    public function setUserAuth($user){

        $frase = $user->id . \Str::random(10) . time();

        $this->userAuth = $user;
        $this->userAuth->api_token = hash('sha256', $frase,false);
        $this->userAuth->forceSave();

    }
    
    public function getUserAuthTransformer(){
        
        $resource = new Item( $this->getUserAuth(), new UserTransformer);
        $fractal = \App::make('League\Fractal\Manager');
        $rootScope = $fractal->createData($resource);
        $data = $rootScope->toArray();
        return array_get($data, 'data');
    }

    public function getUserRegistrado() {
        return $this->userRegistrado;
    }


    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function remind($email)
    {
        $data = ['email' => $email];

        switch ($response = Password::remind($data))
        {
            case Password::INVALID_USER:
                $this->frase = \Lang::get($response);
                return false;

            case Password::REMINDER_SENT:
                $this->frase = \Lang::get($response);
                return true;
        }

    }



    /**
     * Handle a POST request to reset a user's password.
     * @var $credentials array
     * $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
       );
     * @return Response
     */
    public function reset($credentials)
    {

        $response = Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response)
        {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                $this->frase = \Lang::get($response);
                return false;

            case Password::PASSWORD_RESET:
                $this->frase = \Lang::get($response);
                return true;
        }

    }

    public function confirme($code_confirmation){

        if(empty($code_confirmation)){
            return false;
        }

        $user = User::where('confirmation_code', $code_confirmation)->where('confirmed', '0')->first();

        if( empty($user) ){
            $this->frase = "Sua conta já foi confirmada";
            return false;
        }

        $user->confirmation_code = null;
        $user->confirmed = 1;

        if ( $user->save() ) {

            \Event::fire('auth.confirmed', $user);
            $this->frase = "Obrigado por confirmar sua conta. Faça seu login para continuar!";
            return true;

        } else {

            $this->frase = "Não foi possível confirmar seu cadastro";
            return false;

        }

    }

    /**
     * @return mixed
     */
    public function getFrase()
    {
        return $this->frase;
    }

    

}
