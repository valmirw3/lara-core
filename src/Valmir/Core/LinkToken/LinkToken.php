<?php

namespace Valmir\Core\LinkToken;

use Carbon\Carbon;

class LinkToken {

    const PRIVATE_KEY = "VBS@!4#l49212311dfc2005b954525e93256d634e95";

    public function createHash($message){
        return hash_hmac('sha1', $message, self::PRIVATE_KEY);
    }

    public function checkHash($message, $hash){
        $xhash = $this->createHash($message);

        if($xhash === $hash){
            return true;
        }

        return false;
    }

    public function checkHashExpirationDate($message, $hash, $nDias = 10, $time = null){

        $time = (is_null($time))? \Input::get('t') : $time;
        $xhash = $this->createHash($message.$time);

        if($xhash !== $hash){
            return false;
        }

        $date = Carbon::createFromTimestamp($time);
        if( $date->diffInDays() > $nDias  ){
            return false;
        }

        return true;
    }

    public function checkOrFail($message, $hash){
        if( $this->checkHash($message, $hash) === true ){
            return true;
        }

        \App::abort(404, 'Token invalído');
    }

    public function checkExpirationDateOrFail($message, $hash, $nDias = 10, $time = null){

        if( $this->checkHashExpirationDate($message, $hash, $nDias, $time) === true ){
            return true;
        }

        \App::abort(404, 'Token invalído');
    }

    public function route($nameRoute, $message){
        $hash= $this->createHash($message);
        return \URL::route($nameRoute, [$message, $hash]);
    }

    public function routeWithExpirationDate($nameRoute, $message){
        $time = Carbon::now()->getTimestamp();
        $hash = $this->createHash($message . $time);

        $url = \URL::route($nameRoute, [$message, $hash]);

        return $url . '?t=' . $time;
    }

    public function routeAction($nameAction, $message){
        $hash= $this->createHash($message);
        return \URL::action($nameAction, [$message, $hash]);
    }

    public function routeActionWithExpirationDate($nameAction, $message){
        $time = Carbon::now()->getTimestamp();
        $hash = $this->createHash($message . $time);

        $url = \URL::action($nameAction, [$message, $hash]);

        return $url . '?t=' . $time;
    }

}