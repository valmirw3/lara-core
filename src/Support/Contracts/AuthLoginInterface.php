<?php namespace Valmir\Core\Support\Contracts;

interface AuthLoginInterface {

    /**
     * @param array $credentials [nome => '', email => '']
     */
    public function login($credentials);

    /**
     * @param $user
     * @return bool
     */
    public function loginUser($user);
    public function registrar($data);
    public function logout();
    public function getUserAuth();
    public function getUserAuthTransformer();

    /**
     * @return boolean true is valid
     */
    public function validate($data);
    
    public function getErroValidate();
    public function getUserRegistrado();
    
}
