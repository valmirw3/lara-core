<?php
/**
 * Created by PhpStorm.
 * User: valmir
 * Date: 01/12/14
 * Time: 11:48
 */
namespace Valmir\Core\Support;


use League\Fractal\Pagination\PaginatorInterface;

class CorePaginatorAdapter implements  PaginatorInterface{

    /**
     * @var \Illuminate\Pagination\Paginator
     */
    protected $collection;

    function __construct($collection)
    {
        $this->collection = $collection;
        $this->collection->appends(\Input::except('page'));
    }

    public function getCurrentPage()
    {
        return $this->collection->getCurrentPage();
    }

    public function getLastPage()
    {
        return $this->collection->getLastPage();
    }

    public function getTotal()
    {
        return $this->collection->getTotal();
    }

    public function count()
    {
        return $this->collection->count();
    }

    public function getPerPage()
    {
        return $this->collection->getPerPage();
    }

    public function getUrl($page)
    {
        return $this->collection->getUrl($page);
    }
}