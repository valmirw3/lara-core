<?php namespace Valmir\Core\Support\Facades;

use Illuminate\Support\Facades\Facade;

class ResponseCorsFacade extends Facade {

    protected static function getFacadeAccessor() { return 'response_cors'; }

}