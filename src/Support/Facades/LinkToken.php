<?php namespace Valmir\Core\Support\Facades;

use Illuminate\Support\Facades\Facade;

class LinkToken extends Facade {

    protected static function getFacadeAccessor() { return 'link_token'; }

}