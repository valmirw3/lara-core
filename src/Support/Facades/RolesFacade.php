<?php namespace Valmir\Core\Support\Facades;

use Illuminate\Support\Facades\Facade;

class RolesFacade extends Facade {

    protected static function getFacadeAccessor() { return 'core_role'; }

}