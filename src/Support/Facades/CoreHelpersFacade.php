<?php namespace Valmir\Core\Support\Facades;

use Illuminate\Support\Facades\Facade;

class CoreHelpersFacade extends Facade {

    protected static function getFacadeAccessor() { return 'core_helpers'; }

}