<?php namespace Valmir\Core\Support\Traits;

use Illuminate\Support\MessageBag;
use Validator;

trait TraitValidate {
    /**
     * Error message bag
     *
     * @var \Illuminate\Support\MessageBag
     */
    public $errors;
    
    
    protected $forceValidate = false;

    /**
     * Hook into the model boot seqeuence and register events that
     * will allow for model validation.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if ( ! $model->isValid()) return false;
            $model->beforeSave($model);
        });

        static::updating(function($model)
        {
            if ( ! $model->isValid($model->removeUniquenessValidations()) ) return false;
            $model->beforeSave($model);
        });

        static::updated(function($model){
            $model->afterUpdate($model);
        });

        static::saved(function($model){
            $model->afterSave($model);
        });

    }

    /**
     * Return validation errors.
     *
     * @return MessageBag
     */
    public function errors() 
    {
        if(is_null($this->errors)){
            $this->errors = new MessageBag();
        }
        return $this->errors; 
    }

    /**
     * Test to see if model is valid against provided rules.
     *
     * @param  array $rules
     * @return boolean
     */
    public function isValid(array $rules = array()) 
    {

        if( $this->forceValidate ){
            return true;
        }

        if( $this->beforeValidate() === false){
            return false;
        }
        
        $rules = empty($rules) ? static::$rules : $rules;
        $custonMessages = isset(static::$custonMessages)? static::$custonMessages : array();
        
        $validation = Validator::make($this->getAttributes(), $rules, $custonMessages);

        $validation->setAttributeNames(isset(static::$attributesName) ? static::$attributesName : array());

        if ($validation->fails())
        {
            $this->errors = $validation->messages();
            return false;
        }

        return true;
    }


    /**
     * Remove uniqueness validations from the ruleset.
     *
     * @return mixed
     */
    public function removeUniquenessValidations()
    {
        $rules = (isset(self::$rules_update))? self::$rules_update : self::$rules;

        foreach($rules as $field => &$ruleset)
        {
            
            $ruleset = is_string($ruleset) ? explode('|', $ruleset) : $ruleset;

            // Look for, and remove unique validations
            foreach($ruleset as $key => &$current_rule)
            {                
                if (strstr($current_rule, 'unique')) { 
                    $id = (int) $this->attributes[ $this->primaryKey ];                    
                    $current_rule =  "unique:{$this->getTable()},{$field},{$id}";                    
                }
            }
            
        }

        return $rules;
    }

    /**
     * @return Void
     */
    public function beforeValidate(){}


    /**
     * @return Void
     */
    public function beforeSave(){}

    /**
     * @return Void
     */
    public function afterSave(){}

    /**
     * @return Void
     */
    public function afterUpdate(){}

    
    public function forceSave(){
        $this->forceValidate = true;
        $r = $this->save();
        $this->forceValidate = false;
        return $r;
    }
    
}
