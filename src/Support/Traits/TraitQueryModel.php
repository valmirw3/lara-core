<?php

namespace Valmir\Core\Support\Traits;

/**
 * Class TraitQueryModel
 * @package Valmir\Core\Libraries
 * Sua funao � auxiliar com as classes repositories com os metodos mais comuns
 * Obs: N�o deve ser usanda dentro do model diretamente
 */
trait TraitQueryModel{


    public function find($id){
        return $this->model->find($id);
    }

    public function findOrFail($id){
        return $this->model->findOrFail($id);
    }


    /**
     * Otimiza as query do eloquente adicionando
     * Eager Loading do Eloquent
     * @param string cidade,cidade.estado
     * @return \Eloquent
     */
    public function withEmbed($embed = null){

        $embed = (is_null($embed))? \Input::get('embed') : $embed;
        $requestedEmbeds = explode(',', $embed);
        $possibleRelationships = (array) $this->model->relationships;

        $eagerLoad = array_values(
            array_intersect( $possibleRelationships, $requestedEmbeds)
        );

        if( empty($eagerLoad) ) {
            return $this->model->newQuery();
        }
        
        return $this->model->with( $eagerLoad );
    }

    public function scopePaginate($query, $paginate, $orderBy=null, $order = 'ASC' )
    {

        if( ! is_null($orderBy) ){
            $query->orderBy($orderBy, $order);
        }

        if($paginate > 0){
            return $query->paginate($paginate);
        }

        return $query->get();
    }

}