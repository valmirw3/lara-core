<?php

Route::options('service/{var1?}/{var2?}/{var3?}/{var4?}/{var5?}/{var6?}', function() {
    return ResponseCors::responseRouteOptions();
});


Route::group(array('prefix' => 'service/v1', 'namespace' => 'Valmir\Core', 'after' => 'cors'), function() {    
    Route::get('authenticate/logout', 'AuthenticationController@logout');
    Route::get('authenticate/user', 'AuthenticationController@getUser');
    Route::post('authenticate/login', 'AuthenticationController@login');
    Route::post('authenticate/registrar', 'AuthenticationController@registrar');
    Route::post('password/remind', 'AuthenticationController@remind');
    Route::post('password/reset', 'AuthenticationController@reset');
});
