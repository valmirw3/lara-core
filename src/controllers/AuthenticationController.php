<?php namespace Valmir\Core;


use Valmir\Core\Support\Contracts\AuthLoginInterface;
use Valmir\Core\Transformer\UserTransformer;
use Input;


class AuthenticationController extends BaseApiController {

    
    /**
     * @var AuthRepositoryInterface
     */
    protected $auth;
    
    public function __construct( AuthLoginInterface $auth ) {
        parent::__construct();
        $this->auth = $auth;
    }
    
    public function getUser(){
        $user = $this->auth->getUserAuth();
        
        if( !$user ){
            return $this->errorForbidden();
        }
        
        return $this->respondWithItem($user, new UserTransformer);
    }

    public function logout() {
        $this->auth->logout();
        return $this->respondStatus('Disconectado!');
    }

    public function login() {
        $credentials = Input::only('email', 'password');

        if ( $this->auth->login($credentials)  ) {
            return $this->respondStatus('Logado com sucesso!', true, [ 'user' => $this->auth->getUserAuthTransformer() ]);
        } else {
            return $this->setStatusCode(401)->respondWithError( 'Authentication failed', self::CODE_WRONG_ARGS);
        }
    }

    public function registrar() {
        
        if( \Config::get('core::core.route_register_public') !== true && \Auth::guest() ){
            return "Somente Logado";
        }
        
        $data = \Input::only('nome', 'email', 'password', 'password_confirmation', 'codigo');
       
        if ( $this->auth->registrar($data) ) {
            return $this->respondStatus('Registrado com sucesso!', true, $this->auth->getUserRegistrado());
        }

        return $this->errorWrongArgs($this->auth->getErroValidate(), 'Falha ao registrar user, tente novamene!');
    }

}
