<?php namespace Valmir\Core;


use Illuminate\Pagination\Paginator;
use Input;
use Response;
use Event;

use \League\Fractal\Resource\Collection;
use \League\Fractal\Resource\Item;
use Valmir\Core\Support\CorePaginatorAdapter;


class BaseApiController extends \Controller
{
    protected $statusCode = 200;

    const CODE_WRONG_ARGS = 'WRONG_ARGS';
    const CODE_NOT_FOUND = 'NOT_FOUND';
    const CODE_INTERNAL_ERROR = 'INTERNAL_ERROR';
    const CODE_UNAUTHORIZED = 'UNAUTHORIZED';
    const CODE_FORBIDDEN = 'FORBIDDEN';
    const CODE_INVALID_MIME_TYPE = 'INVALID_MIME_TYPE';

    public function __construct()
    {
    
        $this->fractal = \App::make('\League\Fractal\Manager');

        // Are we going to try and include embedded data?
        $this->fractal->setRequestedScopes(explode(',', Input::get('embed')));

        $this->fireDebugFilters();
    }
    

    public function fireDebugFilters()
    {
        $this->beforeFilter(function () {
            Event::fire('clockwork.controller.start');
        });

        $this->afterFilter(function () {
            Event::fire('clockwork.controller.end');
        });
    }

    /**
     * Getter for statusCode
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
    
    /**
     * Setter for statusCode
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }
    
    protected function respondWithItem($item, $callback, $onFail = true)
    {
        
        if($onFail === true && is_null($item) ){
            return $this->errorNotFound();
        }
        
        $resource = new Item($item, $callback);
        $rootScope = $this->fractal->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    protected function respondWithCollection($collection, $callback)
    {

        if ($collection instanceof Paginator) {
            return $this->respondWithCollectionPaginate($collection, $callback);
        }

        $resource = new Collection($collection, $callback);
        $rootScope = $this->fractal->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }
    
    protected function toArrayWithCollection($collection, $callback)
    {
        $resource = new Collection($collection, $callback);
        $rootScope = $this->fractal->createData($resource);

        return $rootScope->toArray();
    }

    protected function respondWithCollectionPaginate($collection, $callback)
    {
        $resource = new Collection($collection, $callback);
        $resource->setPaginator( new CorePaginatorAdapter($collection) );
        $rootScope = $this->fractal->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    protected function respondWithResource(Collection $resource, array $headers = [])
    {
        $rootScope = $this->fractal->createData($resource);
        return $this->respondWithArray($rootScope->toArray(), $headers);
    }

    protected function respondWithArray(array $array, array $headers = [])
    {
//        $mimeTypeRaw = Input::server('HTTP_ACCEPT', '*/*');
//
//        // If its empty or has */* then default to JSON
//
//        if ($mimeTypeRaw === '*/*') {
//            $mimeType = 'application/json';
//        } else {
//             // You will probably want to do something intelligent with charset if provided.
//            // This chapter just assumes UTF8 everything everywhere.
//            $mimeParts = (array) explode(';', $mimeTypeRaw);
//            $mimeType = strtolower($mimeParts[0]);
//        }

        $mimeType = 'application/json';

        switch ($mimeType) {
            case 'application/json':
                $contentType = 'application/json';
                $content = json_encode($array);
                break;

//            case 'application/x-yaml':
//                $contentType = 'application/x-yaml';
//                $dumper = new YamlDumper();
//                $content = $dumper->dump($array, 2);
//                break;

            default:
                $contentType = 'application/json';
                $content = json_encode([
                    'error' => [
                        'code' => static::CODE_INVALID_MIME_TYPE,
                        'http_code' => 415,
                        'message' => sprintf('Content of type %s is not supported.', $mimeType),
                    ]
                ]);
        }

        $response = Response::make($content, $this->statusCode, $headers);
        $response->header('Content-Type', $contentType);

        return $response;
    }

    protected function respondCreateItemSucess($item, $callback, $msg = "Registrado com sucesso!")
    {
        $this->statusCode =  201;
        $resource = new Item($item, $callback);
        $rootScope = $this->fractal->createData($resource);
        $data = $rootScope->toArray();
        $data['message'] = $msg;
        return $this->respondWithArray($data);
    }

    protected function respondUpdateItemSucess($item, $callback, $msg = "Atualizado com sucesso!")
    {
        $this->statusCode =  200;
        $resource = new Item($item, $callback);
        $rootScope = $this->fractal->createData($resource);
        $data = $rootScope->toArray();
        $data['message'] = $msg;
        $data['status'] = 'success';
        return $this->respondWithArray($data);
    }

    protected function respondWithError($message, $errorCode, $validation = null)
    {
        if ($this->statusCode === 200) {
            trigger_error(
                "Erro interno, não especificado o codigo do status em respondWithError.",
                E_USER_WARNING
            );
        }

        return $this->respondWithArray([
            'error' => [
                'code' => $errorCode,
                'http_code' => $this->statusCode,
                'message' => $message,
                'validation' => $validation
            ]
        ]);
    }

    /**
     * Padrão de retorno das requisições
     * @param string $message mensagem de resposta
     * @param booleam $status error/success
     * @param array $erros Validation
     */
    protected function respondStatus($message, $status = true, $data = array(), $erros = array() ){
        $status = ($status)? 'success' : 'error';
        $erros  =  (count($erros))? ['validation' => $erros] : null;
        $result = array('status' => $status, 'message' => $message, 'errors' => $erros, 'data' => $data );
        return $this->respondWithArray($result);
    }
    
    /**
     * Generates a Response with a 403 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorForbidden($message = 'Não autorizado, sem permissão de acesso')
    {
        return $this->setStatusCode(403)->respondWithError($message, self::CODE_FORBIDDEN);
    }

    /**
     * Generates a Response with a 500 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorInternalError($message = 'Erro interno de servidor, desculpe :( ')
    {
        return $this->setStatusCode(500)->respondWithError($message, self::CODE_INTERNAL_ERROR);
    }
    
    /**
     * Generates a Response with a 404 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorNotFound($message = 'Não encontrado')
    {
        return $this->setStatusCode(404)->respondWithError($message, self::CODE_NOT_FOUND);
    }

    /**
     * Generates a Response with a 401 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorUnauthorized($message = 'Não logado')
    {
        return $this->setStatusCode(401)->respondWithError($message, self::CODE_UNAUTHORIZED);
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @param array $validation MessageBag validation
     * @param string $message frase do erro
     * @param string $code padrão WRONG_ARGS
     * @return \Illuminate\Http\Response
     */
    public function errorWrongArgs($validation = null, $message = 'Erro de validação', $code = null)
    {
        $code = (is_null($code))? self::CODE_WRONG_ARGS : $code;

        if(is_string($validation)){
            $message    = $validation;
            $validation = null;
        }

        return $this->setStatusCode(400)->respondWithError($message, $code, $validation);
    }
}
