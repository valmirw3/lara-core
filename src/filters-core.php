<?php
/*
|--------------------------------------------------------------------------
| Registra eventos
|--------------------------------------------------------------------------
|
| Todos os eventos do sistema devem ser registrado
|
*/
Event::subscribe( new Valmir\Core\Events\SubscribeHandler );

/*
|--------------------------------------------------------------------------
| Check Permission Entrust
|--------------------------------------------------------------------------
|
| Verifica se user tem permissão de acessa aquela action,
| caso não tenha, o sistema aborta causando um erro 404
|
| @param string $permission
|
*/
if( ! function_exists('checkPermission') )
{

    function checkPermission($permission)
    {
        if (!Entrust::can($permission)) {
            App::abort(403, "Solicite para o webmaster adicionar a permissão ($permission) ao seu usuário.");
        }
    }

}

/*
|--------------------------------------------------------------------------
| Check array Permission Entrust
|--------------------------------------------------------------------------
|
| Verifica se user tem TODAS as permissão da lista permissions
|
| @param array $permissions
|
*/
if( ! function_exists('checkInArrayPermission') )
{

    function checkInArrayPermission( array  $permissions)
    {
        foreach($permissions as $can)
        {
            if(! Entrust::can($can) ){
                $permission = implode("," , $permissions);
                App::abort(403, "Solicite para o webmaster adicionar todas estas permissões ($permission) ao seu usuário.");
            }
        }

    }

}


/*
|--------------------------------------------------------------------------
| Check array Permission Entrust
|--------------------------------------------------------------------------
|
| Verifica se user tem pelo menos UMA permissão da lista permissions
|
| @param array $permissions
|
*/
if( ! function_exists('checkHasArrayPermission') )
{

    function checkHasArrayPermission( array $permissions)
    {
        foreach ($permissions as $can) {
            if (Entrust::can($can)) {
                return true;
            }
        }

        $permission = implode("," , $permissions);
        App::abort(403, "Solicite para o webmaster adicionar uma das permissões ($permission) ao seu usuário.");
    }

}


/*
|--------------------------------------------------------------------------
| Trata rotas com erros
|--------------------------------------------------------------------------
|
| Quando em ambiente de produção,
| O sistema se encarregara de tratar os erros, em uma tela mais amigavel.|
|
*/

App::error(function(Exception $exception, $code) {

    if (Config::get('app.debug')) {
        return;
    }

    if ( $code === 404 && $exception instanceof Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
        //não registra log de paginas 404
    }else{
        Log::error($exception);
        Event::fire('core::error.send', array( $exception, $code) );
    }

    if(  $code === 500 && $exception instanceof Illuminate\Session\TokenMismatchException){
        return Response::view(Config::get('core::core.view_errors.tokenException'), array(), $code);
    }

    switch ($code) {
        case 403:
            return Response::view( Config::get('core::core.view_errors.403'), array('message' => $exception->getMessage()), 403);

        case 404:
            return Response::view(Config::get('core::core.view_errors.404'), array(), $code);

        case 500:
            return Response::view(Config::get('core::core.view_errors.error'), array(), $code);

        default:
            return Response::view(Config::get('core::core.view_errors.404'), array(), $code);

    }

});


/*
  |--------------------------------------------------------------------------
  | Filter the services
  |--------------------------------------------------------------------------
  |
  | Filtro utilizadas nas respostas REST JSON
  |
 */
Route::filter('serviceAuth', function($request) {
    
    if ( Auth::guest() ) {
        return ResponseCors::make('Acesso negado, favor efetue o login!', 401);
    }
    
});



Route::filter('serviceToken', function($route, $request) {
    
    if ( Auth::guest() ) {
        return ResponseCors::make('Acesso negado, favor efetue o login!', 401);
    }
    
    $payload = $request->header('ng-csrf-token');
    
    $user = Auth::user();

    if(!$payload || !$user || $payload !== $user->api_token )
    {
        return ResponseCors::make('Not authenticated token.', 401);
    }

});

Route::filter('serviceTokenCookie', function($route, $request)
{
    if ( Auth::guest() ) {
        return ResponseCors::make('Acesso negado, favor efetue o login!', 401);
    }
     
    $payload = $request->cookie('webapp_token');   
    $user = Auth::user();

    if( App::environment() === "local" )
    {
            //ignore testes
    }
    else if(!$payload || !$user || $payload !== $user->api_token )
    {
        return ResponseCors::make('Not authenticated token cookie.', 401);
    }        
});


Route::filter('serviceRole', function($route, $request, $role = "admin")
{

    if ( ! CoreRole::hasRole($role))
    {

        $response = [
            "error" => [
                "code"      => "FORBIDDEN",
                "http_code" => 403,
                "message"   => "Necessita de permissão nivel (Role:$role)",
            ]
        ];

        return ResponseCors::make($response, 403);
    }

});

Route::filter('serviceCan', function($route, $request, $can = "admin")
{

    if ( ! CoreRole::can($can))
    {

        $response = [
            "error" => [
                "code"      => "FORBIDDEN",
                "http_code" => 403,
                "message"   =>  "Necessita de permissão (Can:$can)",
            ]
        ];

        return ResponseCors::make($response, 403);
    }

});

Route::filter('serviceCSRF', function() {

    $methods = array('POST', 'PUT', 'DELETE');
    $token = Session::token();
    if ( in_array(Request::getMethod(), $methods) && ( $token != Input::get('_token') && $token  !== Request::header('ng-csrf-token') ) ) {
        return ResponseCors::make('ERROR CSRF', 418);
    }
    
});


Route::filter('cors', function($route, $request, $response, $env=null) {
    
    //Verifica se foi difinido um ambiente onde devera funcionar o cors, 
    //digamos que vc precise que o cors funcione apenas no ambiente de desenvolvimento
    //Pasta usar (after=> 'cors:local')
    if( is_null($env) === FALSE && App::environment($env) === FALSE ){
        return $response;
    }
    
    $origen = (isset($_SERVER['HTTP_ORIGIN']))? $_SERVER['HTTP_ORIGIN'] : '*';
    $response->headers->set('Access-Control-Allow-Origin', "{$origen}");
    $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With, Origin, Authorization, Cache-Control, Content-Type, ng-csrf-token, X-UNIDADE, MODULE');
    $response->headers->set('Access-Control-Allow-Credentials', 'true');
    $response->headers->set('Access-Control-Request-Method:', 'POST, GET, PUT, DELETE');
    return $response;
});




       