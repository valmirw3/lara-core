<?php


return array(

    'unique_cidade' => ':attribute já encontra-se cadastrada nesse estado',
    'cpf' => 'Documento :attribute inválido',
    'cnpj' => 'Documento :attribute inválido',
    'alpha_string' => 'O :attribute só pode conter letras.',
    'data_idade' => 'Idade inválida.',

);