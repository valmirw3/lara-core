<?php namespace Valmir\Core\Transformer;

use User;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableEmbeds = [
    //    'checkins'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        
        return [
            'id'             => (int) $user->id,
            'nome'           => $user->nome,
            'email'          => $user->email,
            'token'          => $user->api_token,
            'created_at'     =>  $user->created_at->format('d/m/Y H:i'),
            'updated_at'     =>  $user->updated_at->format('d/m/Y H:i'),
        ];
    }

    /**
     * Embed Checkins
     *
     * @return League\Fractal\Resource\Collection
     */
//    public function embedCheckins(User $user)
//    {
//        $checkins = $user->checkins;
//
//        return $this->collection($checkins, new CheckinTransformer);
//    }
}
