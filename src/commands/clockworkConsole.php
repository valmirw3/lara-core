<?php

namespace Valmir\Core\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class clockworkConsole extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'clockwork:tail';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Monitora logs co Clockwork.';

    /**
     * The table helper set.
     *
     * @var \Symfony\Component\Console\Helper\TableHelper
     */
    protected $table;

    /**
     * @var ClockworkFiles
     */
    protected $clockworkFiles;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

        $this->clockworkFiles = new ClockworkFiles();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $this->table = $this->getHelperSet()->get('table');
        $this->call('clockwork:clean');
		$arg = $this->argument('list_type');

        switch($arg)
        {
            case 'db':
                $this->tailDB();
        }

	}

    protected function tailDB()
    {

        $columns = ['query', 'duration'];
//        $this->table->setHeaders( $columns );

        while(true){
            if($this->clockworkFiles->hasNewFile()){
                $data = $this->clockworkFiles->getLastFile();
                $this->printHeader($data);
                $queries= (array) $data['databaseQueries'];

                foreach($queries as $q) {
                    //$this->table->addRow( array_only($q, $columns) );
                    $this->info('------------------------------------------------------------------------------------------------------------------------');
                    $this->info($q['query']);
                }

                //$this->table->render($this->getOutput());
            }
            sleep(3);
        }
    }

    protected function printHeader($data){
        $this->info('');
        $this->error("time: " . date('d/m/Y H:i:s', array_get($data, 'time')));
        $this->error("method: " . array_get($data, 'method'));
        $this->error("uri: " . array_get($data, 'uri'));
        $this->error("controller: " . array_get($data, 'controller'));
        //$this->error("responseTime: " . date('s', array_get($data, 'responseTime')) );
        $this->error("databaseDuration: " . array_get($data, 'responseDuration'));
        $this->error("total queries: " . count( array_get($data, 'databaseQueries')) );

    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('list_type', InputArgument::OPTIONAL, 'Type de Lista: db,timeline'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		//	array('db', null, InputOption::VALUE_OPTIONAL, 'Lista query database.', null),
		);
	}

}

class ClockworkFiles{

    protected $path;

    /**
     * @var \Directory
     */
    protected $dir;

    protected $last;

    protected $files = [];

    function __construct()
    {
        $this->path = storage_path('clockwork');
    }

    public function read(){
        $this->dir = dir($this->path);
        $this->last = null;

        while($arquivo = $this->dir->read() ){

            if( in_array($arquivo, ['.', '..']) === false && in_array($arquivo, $this->files) === false){
                $this->last = $arquivo;
                array_push($this->files, $arquivo);
            }

        }
    }

    public function hasNewFile()
    {
        $this->read();
        return ( is_null($this->last ) )? false : true;
    }

    public function getLastFile()
    {
        $json_file = file_get_contents($this->path . '/' . $this->last);
        return json_decode($json_file, true);
    }

}
